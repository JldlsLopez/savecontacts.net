package com.JLopez.saveContacts.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "users")
@NamedQueries({ @NamedQuery(name = User.GET_USER, query = "Select u from User u where u.id = :userId"),
		@NamedQuery(name = User.GET_ONE, query = "Select u from User u where u.userName = :username"),
		@NamedQuery(name = User.GET_USERS_LIST, query = "Select u from User u"),
		@NamedQuery(name = User.GET_ALL_MAILS, query = "Select u.mail from User u"),
		@NamedQuery(name = User.GET_ALL_USERS_NAME, query = "Select u.userName from User u"),
		@NamedQuery(name = User.GET_USER_BY_EMAIL, query = "Select u from User u where u.mail = :userEmail") })
public class User {

	public static final String GET_USER = "getUser";

	public static final String GET_ONE = "getOne";

	public static final String GET_USERS_LIST = "getUsersList";

	public static final String GET_ALL_USERS_NAME = "getUsersName";

	public static final String GET_ALL_MAILS = "getMails";

	public static final String GET_USER_BY_EMAIL = "getUserByEmail";

	public static String getGetAllMails() {
		return GET_ALL_MAILS;
	}

	public static String getGetAllUsersName() {
		return GET_ALL_USERS_NAME;
	}

	public static String getGetOne() {
		return GET_ONE;
	}

	public static String getGetUser() {
		return GET_USER;
	}

	public static String getGetUserByEmail() {
		return GET_USER_BY_EMAIL;
	}

	public static String getGetUsersList() {
		return GET_USERS_LIST;
	}

	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Expose
	private int id;
	@Column(name = "username")
	@Expose
	private String userName;
	@Expose
	private String mail;
	@Expose
	@Column(name = "password")
	private String psw;
	private boolean enabled = true;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Expose
	private List<Authority> authoritiesList = new ArrayList<Authority>();

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Expose
	private List<Contact> contactsList = new ArrayList<Contact>();

	public List<Authority> getAuthoritiesList() {
		return authoritiesList;
	}

	public List<Contact> getContactsList() {
		return contactsList;
	}

	public int getId() {
		return id;
	}

	public String getMail() {
		return mail;
	}

	public String getPsw() {
		return psw;
	}

	public String getUserName() {
		return userName;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setAuthoritiesList(List<Authority> authoritiesList) {
		this.authoritiesList = authoritiesList;
	}

	public void setContactsList(List<Contact> contactsList) {
		this.contactsList = contactsList;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setPsw(String psw) {
		this.psw = psw;
	}

	public void setUserName(String usrName) {
		this.userName = usrName;
	}

}
