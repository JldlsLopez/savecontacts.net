package com.JLopez.saveContacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "authorities")
public class Authority {
	@Id
	@GeneratedValue
	@Expose
	private int id;
	@Column(name = "username")
	@Expose
	private String userName;
	@Expose
	private String authority;

	@ManyToOne
	private User user;

	public String getAuthority() {
		return authority;
	}

	public int getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public String getUserName() {
		return userName;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
