package com.JLopez.saveContacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

@Entity
public class Number {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Expose
	private int id;
	@Expose
	private String number;

	@ManyToOne
	private Contact contact;

	public Contact getContact() {
		return contact;
	}

	public int getID() {
		return id;
	}

	public String getNumber() {
		return number;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setID(int iD) {
		id = iD;
	}

	public void setNumber(String number) {
		this.number = number;
	}

}
