package com.JLopez.saveContacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

@Entity
public class Address {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Expose
	private int id;
	@Expose
	private String street;
	@Expose
	private String city;
	@Expose
	private String state;
	@Expose
	private String zypCode;

	@ManyToOne
	private Contact contact;

	public int getId() {
		return id;
	}

	public void setId(int iD) {
		id = iD;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZypCode() {
		return zypCode;
	}

	public void setZypCode(String zypCode) {
		this.zypCode = zypCode;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
}
