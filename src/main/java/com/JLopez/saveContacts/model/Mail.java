package com.JLopez.saveContacts.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

@Entity
public class Mail {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Expose
	private int id;
	@Expose
	private String mail;

	@ManyToOne
	private Contact contact;

	public int getID() {
		return id;
	}

	public void setID(int iD) {
		id = iD;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
}
