package com.JLopez.saveContacts.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

@Entity
public class Contact {

	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Expose
	private int id;
	@Expose
	private String name;
	@Expose
	private String surname;
	@Expose
	private String gender;
	@OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
	@Expose
	private List<Number> numberList = new ArrayList<Number>();
	@OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
	@Expose
	private List<Address> addressList = new ArrayList<Address>();
	@OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
	@Expose
	private List<Mail> mailList = new ArrayList<Mail>();
	@ManyToOne
	private User user;

	public List<Address> getAddressList() {
		return addressList;
	}

	public String getGender() {
		return gender;
	}

	public int getId() {
		return id;
	}

	public String getSurname() {
		return surname;
	}

	public List<Mail> getMailList() {
		return mailList;
	}

	public String getName() {
		return name;
	}

	public List<Number> getNumberList() {
		return numberList;
	}

	public User getUser() {
		return user;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setMailList(List<Mail> mailList) {
		this.mailList = mailList;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumberList(List<Number> numberList) {
		this.numberList = numberList;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
