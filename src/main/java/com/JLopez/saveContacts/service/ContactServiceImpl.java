package com.JLopez.saveContacts.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.JLopez.saveContacts.model.Contact;
import com.JLopez.saveContacts.repository.ContactRepository;

@Service("contactService")
public class ContactServiceImpl implements ContactService {
	
	@Autowired
	ContactRepository contactRepository;

	@Transactional
	public Contact save(Contact contact) {
		
		return contactRepository.saveAndFlush(contact);
	}

	public Contact upDate(Contact contact) {
		
		return contactRepository.save(contact);
	}
	
    public void remove(int contactId) {
		
		contactRepository.delete(contactId);
		contactRepository.flush();
	}
	
}
