package com.JLopez.saveContacts.service;

import java.util.List;

import com.JLopez.saveContacts.model.User;

public interface UserService {

	User save(User user);

	User getUser(User user);

	User getOne(String userName);

	User getUserByEmail(String email);

	User encodeUserPassword(User user);

	List<String> getAllUsersName();

	List<String> getAllMails();
}