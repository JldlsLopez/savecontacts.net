package com.JLopez.saveContacts.service;

import com.JLopez.saveContacts.model.Contact;

public interface ContactService {

	Contact save(Contact contact);

	Contact upDate(Contact contact);

	void remove(int contactId);

}