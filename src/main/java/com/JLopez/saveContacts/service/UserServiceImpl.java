package com.JLopez.saveContacts.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.JLopez.saveContacts.model.User;
import com.JLopez.saveContacts.repository.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public User save(User user) {

		return userRepository.saveAndFlush(user);
	}

	public User getUser(User user) {

		return userRepository.getOne(user.getId());
	}

	public User getOne(String userName) {

		TypedQuery<User> query = em.createNamedQuery(User.GET_ONE, User.class);
		query.setParameter("username", userName);
		return query.getSingleResult();
	}

	public User getUserByEmail(String email) {
		TypedQuery<User> query = em.createNamedQuery(User.GET_USER_BY_EMAIL, User.class);
		query.setParameter("userEmail", email);
		return query.getSingleResult();
	}

	public User encodeUserPassword(User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String decodedPassword = user.getPsw();
		user.setPsw(passwordEncoder.encode(decodedPassword));
		return user;

	}

	public List<String> getAllUsersName() {
		TypedQuery<String> query = em.createNamedQuery(User.GET_ALL_USERS_NAME, String.class);

		return query.getResultList();
	}

	public List<String> getAllMails() {
		TypedQuery<String> query = em.createNamedQuery(User.GET_ALL_MAILS, String.class);

		return query.getResultList();
	}

}
