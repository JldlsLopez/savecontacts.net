package com.JLopez.saveContacts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.JLopez.saveContacts.model.Contact;

@Repository("contactRepository")
public interface ContactRepository extends JpaRepository<Contact, Integer> {

}
