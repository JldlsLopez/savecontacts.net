package com.JLopez.saveContacts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.JLopez.saveContacts.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Integer> {
}