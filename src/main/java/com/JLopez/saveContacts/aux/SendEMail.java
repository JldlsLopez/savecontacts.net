package com.JLopez.saveContacts.aux;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class SendEMail {
	
	public void sendEMail(JavaMailSender mailSender, String emailTo, String subject, String text){
		
		SimpleMailMessage email = new SimpleMailMessage();
		
		email.setTo(emailTo);
		email.setSubject(subject);
		email.setText(text);
		mailSender.send(email);
		
	}

}
