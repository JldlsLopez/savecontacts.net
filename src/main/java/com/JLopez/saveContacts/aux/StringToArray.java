package com.JLopez.saveContacts.aux;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class StringToArray {
	public List<String> stringToArray(String string){

    	List<String> stringList = new ArrayList<String>();
    	StringTokenizer st = new StringTokenizer(string, ",");
    	
    	while(st.hasMoreElements()){
    		String nextElement = (String) st.nextElement();
    		stringList.add(nextElement);
    		
    	}
    	return stringList;
    }
}
