package com.JLopez.saveContacts.aux;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringEncrypting {
	
	private static final String CHARACTERS = "$0w4b59t6o7fd!pmk8ighaclx1nq-3rze2yujvs~_.'";

	public static String stringEncrypting(String email){
		email = email.replace('@', '-');
		String emailEncrypted = "";
		String pattern = "(.*)(\\d+)(.*)";
		Pattern patternObj = Pattern.compile(pattern);
		
		for (int i = 0; i < email.length(); i++) {
			String tmpChar = email.substring(i, i+1);
			int tmpPosition = CHARACTERS.indexOf(tmpChar);
			if (tmpPosition != -1) {
				String tmpCharEncrypted = 
						CHARACTERS.substring(tmpPosition-1, tmpPosition) + 
						CHARACTERS.substring(tmpPosition+1, tmpPosition+2);
				Matcher matcher = patternObj.matcher(tmpCharEncrypted);
				if (!matcher.find()) {
					tmpCharEncrypted = tmpCharEncrypted.toUpperCase();
				}
				
				emailEncrypted += tmpCharEncrypted;
				
			}
		}
		return emailEncrypted;
	}
	
	
	public static String stringDesencrypting(String emailEncrypted){
		
		String email= "";
		
		for (int i = 0; i < emailEncrypted.length(); i+=2) {
			String tmpChar = emailEncrypted.substring(i, i+2);
			tmpChar = tmpChar.substring(0, 1).toLowerCase();
			int tmpPosition = CHARACTERS.indexOf(tmpChar);
			if (tmpPosition != -1) {
				String tmpCharDesencrypted = CHARACTERS.substring(tmpPosition+1, tmpPosition+2);
				
				email += tmpCharDesencrypted;
			}
		}
		email = email.replace('-', '@');
		return email;
	}

}
