package com.JLopez.saveContacts.controller;

import java.util.List;

import javax.persistence.PersistenceException;

import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.JLopez.saveContacts.aux.SendEMail;
import com.JLopez.saveContacts.model.Authority;
import com.JLopez.saveContacts.model.User;
import com.JLopez.saveContacts.service.UserService;

@Controller
public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	JavaMailSender mailSender;

	@RequestMapping(value = "/signUp", method = RequestMethod.GET)
	public String signUpGet(@ModelAttribute("User") User user, Model model) {

		return "SignUp";
	}

	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public String signUpPost(@ModelAttribute("User") User user, Model model) {
		boolean userSaved = true;

		Authority authority = new Authority();
		user.setId(0);
		user = userService.encodeUserPassword(user);
		try {

			authority.setId(0);
			authority.setUserName(user.getUserName());
			authority.setAuthority("ROLE_USER");
			authority.setUser(user);

			user.getAuthoritiesList().add(authority);
			userService.save(user);

		} catch (JDBCConnectionException jdbce) {

			userSaved = false;
		} catch (PersistenceException pe) {

			userSaved = false;
		} catch (CannotCreateTransactionException ccte) {

			userSaved = false;
		}

		if (userSaved) {
			sendWelcomeEmail(user.getMail(), model);
		}

		model.addAttribute("email", user.getMail());
		model.addAttribute("requestJsp", "done");
		return "Home";
	}

	@RequestMapping(value = "/FBsignup", method = RequestMethod.POST)
	public @ResponseBody String fbSignUp(@RequestParam String id, @RequestParam String first_name,
			@RequestParam String last_name, @RequestParam String email, @RequestParam boolean justLogin, Model model) {
		String userName = first_name + last_name;
		userName = userName.replaceAll(" ", "");
		boolean userNameIsSaved = false;
		List<String> users = userService.getAllUsersName();

		for (String tmpUser : users) {
			if (tmpUser.equalsIgnoreCase(userName)) {

				userNameIsSaved = true;
				justLogin = true;

			}
		}

		if (!userNameIsSaved && justLogin == false) {
			User user = new User();
			user.setId(0);
			user.setPsw(id);
			user.setMail(email);
			user.setUserName(userName);
			user = userService.encodeUserPassword(user);

			Authority authority = new Authority();

			authority.setId(0);
			authority.setUserName(user.getUserName());
			authority.setAuthority("ROLE_USER");
			authority.setUser(user);

			user.getAuthoritiesList().add(authority);
			userService.save(user);
			userNameIsSaved = true;
			justLogin = true;
			sendWelcomeEmail(user.getMail(), model);

		}
		return "{'userName': '" + userName + "', 'password': '" + id + "', 'justLogin': '" + justLogin
				+ "', 'userNameIsSaved': '" + userNameIsSaved + "'}";
	}

	@RequestMapping(value = "/Done", method = RequestMethod.GET)
	public String DoneGet() {
		return "Done";
	}

	@RequestMapping(value = "/AboutUs", method = RequestMethod.POST)
	public String AboutUsGet() {
		return "AboutUs";
	}

	@RequestMapping(value = "/check_username_availability", method = RequestMethod.GET)
	public @ResponseBody boolean checkUsernameAvailability(@RequestParam String userName) {
		boolean userNameAvailable = true;
		List<String> userNameList = userService.getAllUsersName();
		for (String tmpUserName : userNameList) {

			if (tmpUserName.equalsIgnoreCase(userName)) {
				userNameAvailable = false;
			}

		}
		return userNameAvailable;
	}

	@RequestMapping(value = "/check_mail_availability", method = RequestMethod.GET)
	public @ResponseBody boolean checkMailAvailability(@RequestParam String mail) {
		boolean mailAvailable = true;
		List<String> mailList = userService.getAllMails();
		for (String tmpMail : mailList) {

			if (tmpMail.equalsIgnoreCase(mail)) {
				mailAvailable = false;
			}
		}
		return mailAvailable;
	}

	private void sendWelcomeEmail(String email, Model model) {

		SendEMail sendEmail = new SendEMail();
		String msgSubject = "Welcome to SaveContacts.net";
		String msgBody = "Thank you for testing the correct operation of this project." +

				"\n\nSaveContacts.net is a personal non-commercial project developed in Java for educational purposes, with the objetive of "
				+ "putting into practice the use of the following technologies, frameworks, languages and others:" +

				"\n\nJava, Hibernate ORM, Maven, MySql, Apache Tomcat, Gson" +

				"\n\nHTML - HTML5, CSS - CSS3, Javascript - Jquery framework, BootStrap framework, Ajax" +

				"\n\nSpring Tool Suite IDE, Spring-mvc and Amazon aws." +

				"\n\nFor questions or constructive criticism please write to the email address: jorgeluisdelossantoslopez@gmail.com"
				+

				"\n\nWhatsapp: 1-829-872-4331" +

				"\n\nThis is a generated automatically message, please do not reply to this message." +

				"\n\nThank you.";

		try {
			sendEmail.sendEMail(mailSender, email, msgSubject, msgBody);
		} catch (org.springframework.mail.MailSendException e) {
			System.err.println("Error: " + e);
		}
	}

}
