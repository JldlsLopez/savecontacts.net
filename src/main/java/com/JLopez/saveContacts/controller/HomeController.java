package com.JLopez.saveContacts.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.JLopez.saveContacts.model.User;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/Home", method = RequestMethod.GET)
	public String homeGet(@ModelAttribute("User") User user,  Model model){
		model.addAttribute("requestJsp", "home");
		return "Home";
	}
	
}
