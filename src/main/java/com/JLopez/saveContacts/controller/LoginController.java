package com.JLopez.saveContacts.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.JLopez.saveContacts.aux.SendEMail;
import com.JLopez.saveContacts.aux.StringEncrypting;
import com.JLopez.saveContacts.model.User;
import com.JLopez.saveContacts.service.UserService;

@Controller
public class LoginController {

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginGet(@ModelAttribute("User") User user, ModelMap model) {
		model.addAttribute("requestJsp", "login");
		return "Home";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPost(@ModelAttribute("User") User user) {
		return "Login";
	}

	@RequestMapping(value = "/loginFailed", method = RequestMethod.GET)
	public String loginFailed(@ModelAttribute("User") User user, ModelMap model) {
		model.addAttribute("error", "true");
		model.addAttribute("requestJsp", "login");
		return "Home";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout() {

		return "Home";
	}

	@RequestMapping(value = "/request_reset_login_user_password", method = RequestMethod.POST)
	public @ResponseBody String requestResetPasswordPost(@RequestParam String emailForRecover, Model model) {
		String encryptedEmail = StringEncrypting.stringEncrypting(emailForRecover);
		String href = "http://savecontacts.us-west-2.elasticbeanstalk.com/reset_login_user_password.html?userkey=" + encryptedEmail;

		SendEMail sender = new SendEMail();
		String subject = "SaveContacts.net: Request for password recovery";
		String text = "We have received a password recovery request from your email address, if you want to recover access"
				+ " to your account, please click on the following link:" + "\n\n" + href
				+ "\n\nOtherwise, please ignore this email"
				+ "\n\nThis is a generated automatically message, please do not reply to this message."
				+ "\n\nThank you.";

		String response = "";

		try {
			sender.sendEMail(mailSender, emailForRecover, subject, text);
			response = "success";
		} catch (org.springframework.mail.MailSendException e) {

			response = "fail";
		}
		response += "," + emailForRecover;
		return response;
	}

	@RequestMapping(value = "/set_new_password", method = RequestMethod.GET)
	public String setNewPasswordGet() {

		return "SetNewPasswordModal";
	}

	@RequestMapping(value = "/reset_login_user_password", method = RequestMethod.GET)
	public String resetPasswordGet(@ModelAttribute("User") User user, @RequestParam String userkey, Model model) {
		model.addAttribute("userkey", userkey);
		model.addAttribute("requestJsp", "setNewPasswordModal");
		return "Home";
	}

	@RequestMapping(value = "/set_new_password", method = RequestMethod.POST)
	public @ResponseBody String resetPasswordPost(@ModelAttribute("User") User us, @RequestParam String psw,
			@RequestParam String userMail) {
		String desencryptedEmail = StringEncrypting.stringDesencrypting(userMail);
		User user = userService.getUserByEmail(desencryptedEmail);
		user.setPsw(psw);
		user = userService.encodeUserPassword(user);
		userService.save(user);
		return "success";
	}

}
