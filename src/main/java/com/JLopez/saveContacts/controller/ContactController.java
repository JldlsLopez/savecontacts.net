package com.JLopez.saveContacts.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.JLopez.saveContacts.aux.StringToArray;
import com.JLopez.saveContacts.model.Address;
import com.JLopez.saveContacts.model.Contact;
import com.JLopez.saveContacts.model.Mail;
import com.JLopez.saveContacts.model.Number;
import com.JLopez.saveContacts.model.User;
import com.JLopez.saveContacts.service.ContactService;
import com.JLopez.saveContacts.service.UserService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class ContactController {

	@Autowired
	ContactService contactService;

	@Autowired
	UserService userService;

	StringToArray st = new StringToArray();

	@RequestMapping(value = "/User", method = RequestMethod.GET)
	public String doGet(Model model) {
        model.addAttribute("requestJsp", "User");
		return "User";
	}

	@RequestMapping(value = "/SaveContacts/get_current_user", method = RequestMethod.GET)
	public @ResponseBody String getCurrentUser(@RequestParam String username, Model model) {
		User user = userService.getOne(username);
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(user);
	}

	@RequestMapping(value = "/SaveContacts/User", method = RequestMethod.POST)
	public @ResponseBody String save(@RequestParam String contactAsJson, @RequestParam String username) {
		User user = userService.getOne(username);
		Gson gson = new Gson();
		Contact contact = gson.fromJson(contactAsJson, Contact.class);
		contact.setUser(user);

		for (Number number : contact.getNumberList()) {
			number.setContact(contact);
		}

		for (Address address : contact.getAddressList()) {
			address.setContact(contact);
		}

		for (Mail mail : contact.getMailList()) {
			mail.setContact(contact);
		}
		contactService.save(contact);

		return "" + contact.getUser().getContactsList().size();
	}

	@RequestMapping(value = "/SaveContacts/update", method = RequestMethod.POST)
	public @ResponseBody String upDate(@RequestParam int contactId, @RequestParam String name,
			@RequestParam String surname, @RequestParam String gender, @RequestParam String[][] numbers,
			@RequestParam String[] addresses, @RequestParam String[][] mails, @RequestParam String currentUserName,
			Model model, HttpSession s) {

		int NumberMiddlePoint = numbers.length / 2;
		String[] numbersArray = new String[NumberMiddlePoint];
		Integer[] numbersIdArray = new Integer[NumberMiddlePoint];

		for (int i = 0; i < numbers.length; i++) {
			if (i < NumberMiddlePoint) {
				System.out.println("fuera: " + numbers[i][0]);
				if (!numbers[i][0].equals("empty")) {
					numbersArray[i] = numbers[i][0];
				} else {
					numbersArray[i] = "";
				}

			} else if (i >= NumberMiddlePoint) {

				numbersIdArray[i - NumberMiddlePoint] = Integer.valueOf(numbers[i][0]);
			}
		}

		Contact contact = new Contact();
		contact.setId(contactId);

		List<Number> numbersList = new ArrayList<Number>();
		List<Address> addressesList = new ArrayList<Address>();
		List<Mail> mailsList = new ArrayList<Mail>();

		for (int i = 0; i < NumberMiddlePoint; i++) {

			Number number = new Number();
			number.setID(numbersIdArray[i]);
			number.setNumber(numbersArray[i]);
			number.setContact(contact);
			numbersList.add(number);

		}

		Address address = new Address();
		address.setId(Integer.valueOf(addresses[0]));
		address.setStreet(addresses[1]);
		address.setCity(addresses[2]);
		address.setState(addresses[3]);
		address.setZypCode(addresses[4]);
		address.setContact(contact);
		addressesList.add(address);

		int emailMiddlePoint = mails.length / 2;
		String[] emailsArray = new String[emailMiddlePoint];
		Integer[] emailsIdArray = new Integer[emailMiddlePoint];

		for (int i = 0; i < mails.length; i++) {

			if (i < emailMiddlePoint) {

				emailsArray[i] = mails[i][0];

			} else if (i >= emailMiddlePoint) {

				emailsIdArray[i - emailMiddlePoint] = Integer.valueOf(mails[i][0]);
			}

		}

		for (int i = 0; i < emailMiddlePoint; i++) {

			Mail mail = new Mail();
			mail.setID(emailsIdArray[i]);
			mail.setMail(emailsArray[i]);
			mail.setContact(contact);
			mailsList.add(mail);
		}

		contact.setName(name);
		contact.setSurname(surname);
		contact.setGender(gender);
		contact.setNumberList(numbersList);
		contact.setAddressList(addressesList);
		contact.setMailList(mailsList);
		contact.setUser(userService.getOne(currentUserName));

		contactService.upDate(contact);

		return "Successfull";
	}

	@RequestMapping(value = "/SaveContacts/remove", method = RequestMethod.POST)
	public @ResponseBody String remove(@RequestParam int contactId, @RequestParam int tableLength, Model model) {

		contactService.remove(contactId);
		if (tableLength > 0) {

			tableLength -= 1;
		}
		return "" + tableLength;
	}
}