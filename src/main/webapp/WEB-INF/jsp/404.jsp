<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html lang="en">
<head>
<title>SaveContacts.net</title>
<link rel="stylesheet" href="assets/css/3DAnimateText.css">
<link rel="stylesheet" href="assets/css/404.css">
<link rel="icon" href="assets/ico/favico.ico">
<link href="https://fonts.googleapis.com/css?family=Nova+Flat|Gloria+Hallelujah|Indie+Flower|Exo|Amita" rel="stylesheet">
</head>
<body>
  <div id="container">
  
    <section>

      <div>
        
   <div id="divOops">
  <div class="stage">
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
  <div class="layer"></div>
</div>
  </div>
   <h1 class="Text3D">404</h1>
        <h3 class="Text3D"><spring:message code="pageNotFound"/></h3>
      </div>
      <p id="p1"><spring:message code="404details"/></p>
      <p class="Text3D"><a href="javascript:history.go(-1)">&laquo; <spring:message code="goBack"/> </a> / 
      <a href="Home.html"><spring:message code="goHome"/> &raquo;</a></p>

    </section>
  </div>
</body>
</html>