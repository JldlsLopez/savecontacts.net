
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<body>
 
  <!-- SignUp modal -->
  <div class="modal fade" id="modal" role="dialog">
  <!--landmark-->
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <a id="xbtnHref"><button id="xbtnCancel" type="button" class="cancel close" data-dismiss="modal">&times;</button></a>
         <span><i class="material-icons" style="font-size: 30px;">lock_outline</i></span> <spring:message code="setNewPassword"/>
        </div>
        <div class="modal-body" id="new_psw_modal_body">
          <form  id="newPasswordForm">
            <div class="form-group">
              <div class="input-group">
      <div class="input-group-addon" id="divAddon"><span id="addon3"><i class="material-icons" style="font-size: 17px;">lock_outline</i></span></div>
      <input type="text" class="form-control psw-field psw-field-recover" id="new_psw" name="newPassword" placeholder="" autofocus/>
      <p id="newPasswordlPlaceholder" class="hiddenElement"><spring:message code="newPasswordlPlaceholder"/></p>
      <input name="userkey" type="text" id="userKeyPsw" class="hiddenElement"/>
      <input type="text" class="errorField" id="new_pswErr" />
      <span class="form-control-feedback" aria-hidden="true"></span>
      
    </div>
            </div>

          </form>
          <h3 id="msgNewPassword" class="hiddenElement"></h3>
        </div>
        <h3 id="passwordChanged" class="hiddenElement"><spring:message code="passwordChanged"/></h3>
        <div class="modal-footer">
          <button type="button" class="btn btn-cancel pull-left cancel" id="newPswCancelBtn" data-dismiss="modal"><span class="glyphicon glyphicon-remove">
          </span><b> <spring:message code="cancel"/></b></button>
          <button type="button" class="btn btn-default btn-success btn-inline" id="newPswSaveBtn">
          <span>
          <i class="material-icons" style="font-size: 17px;">cloud_upload</i>
          </span><b> <spring:message code="save"/></b> </button>
          
        </div>
      </div>
    </div>
    <!--LANDMARK-->
  </div> 
<!-- signUp-->		
</body>
</html>