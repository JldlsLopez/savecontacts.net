
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<!DOCTYPE html>
<html lang="en">
<head>

<title>SaveContacts.net</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/custombootstrap.css">
<link rel="stylesheet" href="assets/css/loading-modal.css">
<link rel="icon" href="assets/ico/favico.ico">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link href="https://fonts.googleapis.com/css?family=Nova+Flat|Gloria+Hallelujah|Indie+Flower|Exo|Dosis|Amita|Cabin+Sketch" rel="stylesheet">
<link rel="stylesheet" href="assets/css/home.css">
<link rel="stylesheet" href="assets/css/validate.css">
</head>
<body>

<!-- Se inicializa con un valor not null si el login del usuario no es correcto -->
      <div class="hiddenElement" id="homeErrDiv">
        <c:if test="${not empty error }">
        <div id="loginError">
       <spring:message code="loginmsg"/>
        </div>
        </c:if>
        </div>
        
<div class="modal fade" id="loading_modal" role="dialog">
   <div class="bubblingG">
	<span id="bubblingG_1">
	</span>
	<span id="bubblingG_2">
	</span>
	<span id="bubblingG_3">
	</span>
</div>
</div>

<p id="hidenRequestJsp" class="hiddenElement">${requestJsp}</p>
 <c:if test="${requestJsp.equals('home')||requestJsp.equals('login')}"><script src="assets/js/facebook-login.js"></script></c:if>

<!-- Bloque de elementos hidden -->

<div id="elementsHiddenBlock" class="hiddenElement">

    <!-- Mensajes de validacion para el formulario de sign up -->
    <p id="signupErrMsg1"><spring:message code="signupErrMsg1"/></p>
    <p id="signupErrMsg2"><spring:message code="signupErrMsg2"/></p>
    <p id="signupErrMsg3"><spring:message code="signupErrMsg3"/></p>
    <p id="signupErrMsg4"><spring:message code="signupErrMsg4"/></p>
    <p id="signupErrMsg5"><spring:message code="signupErrMsg5"/></p>
    <p id="signupErrMsg6"><spring:message code="signupErrMsg6"/></p>
    <p id="signupErrMsg7"><spring:message code="signupErrMsg7"/></p>
    <p id="signupErrMsg8"><spring:message code="signupErrMsg8"/></p>
    <p id="signupErrMsg9"><spring:message code="signupErrMsg9"/></p>
    <p id="signupErrMsg10"><spring:message code="signupErrMsg10"/></p>
    <p id="signupErrMsg11"><spring:message code="signupErrMsg11"/></p>
    <!-- New password -->
    <p id="userKeyHome">${userkey}</p>
    <!-- Mensajes de validacion para el formulario de recover password -->
    <p id="recoverErrMsg"><spring:message code="recoverErrMsg"/></p>
    
    </div>
<!-- Fin bloque de elementos hidden -->
        
<!-- Barra de menus -->
	<nav class="navbar navbar-inverse ">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="Home.html">SaveContacts.net</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li class="active"><a href="Home.html"><span class="glyphicon glyphicon-home"></span> <spring:message code="home"/></a></li>
					<li><a id="btnAboutUsModal" class="custom-a"> <i class="fa fa-group"></i> <spring:message code="AboutUs"/></a></li>
					
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
					  <i class="material-icons" style="font-size: 14px;">contacts</i> <spring:message code="ContactUs"/> <span class="caret"></span></a>
						<ul class="dropdown-menu"> 
						<li> <pre><i class="fa fa-whatsapp" style="font-size:14px; margin-left:5px; color:green;"></i> Whatsapp: 829-872-4331</pre></li>
						<li class="divider"></li>
						<li> <pre><i class="fa fa-envelope-o" style="font-size:14px; margin-left:5px; color:red;"></i> Email: jorgeluisdelossantoslopez@gmail.com</pre></li>
						<li class="divider"></li>
						<li> <pre><i class="fa fa-bitbucket" style="font-size:14px; margin-left:5px;"></i> Bitbucket: <a class="custom-a" href="https://JldlsLopez@bitbucket.org" target="_blank">https://JldlsLopez@bitbucket.org</a></pre></li>
					   </ul>
					 </li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"> <span
							class="glyphicon glyphicon-globe"></span> <spring:message
								code="language"></spring:message> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="?language=en"><spring:message
										code="english"></spring:message></a></li>
										<li class="divider"></li>
							<li><a href="?language=es"><spring:message
										code="spanish"></spring:message></a></li>
						</ul></li>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container"></div>
	<!-- Final barra de menus -->
	
	<div class="jumbotron text-center">
		<h1 class="title">
			<spring:message code="titleMessage"></spring:message>
		</h1>
		<p class="subTittle">
			<spring:message code="subTittle" />
		</p>
	<div id="logoutUser">
	
	<ul class="allInLine">
		<li><button type="button" class="btn btn-primary btn-lg" id="signUpHome"><span><i class="material-icons custom-btn-icon">cloud_upload</i></span>
		<spring:message code="signup"></spring:message></button></li>
		<li id="btn2"><button type="button" id="loginHome" class="btn btn-primary btn-lg"><i class="fa fa-sign-in"></i> 
 	<spring:message code="login"></spring:message></button></li>
				</ul>
	
	</div>
		</div>
		
	<div class="modal fade container" id="modal_div" role="dialog">
	
	 <!-- SignUp modal -->
     <div id="signupModal" class="hiddenElement">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="cancel close" data-dismiss="modal">&times;</button>
         <span><i class="material-icons" style="font-size: 30px;">cloud_upload</i></span> <spring:message code="signup"/>
        </div>
        <div class="modal-body">
          <form:form modelAttribute="User" action="signUp.html" method="post" id="signupForm">
            <div class="form-group">
              <div class="input-group">
              <p class="hiddenElement" id="usrNamePlaceholder"><spring:message code="username"/></p>
      <div class="input-group-addon"><span id="addon1"><i class="fa fa-user"></i></span></div>
      <form:input path="userName" type="text" class="form-control inputField usr-name-field" id="usrName" placeholder=""/>
      <input type="text" class="errorField" id="usrNameErr" />
      <span class="form-control-feedback" aria-hidden="true"></span>
    </div>
            </div>
            
            <div class="form-group">
            <div class="input-group">
            <p class="hiddenElement" id="mailPlaceholder"><spring:message code="email"/></p>
            <div class="input-group-addon"><span id="addon2"><i class="fa fa-envelope"></i></span></div>
              <form:input path="mail" type="mail" class="form-control inputField email-field" id="mail" placeholder=""/>
              <input type="text" class="errorField" id="mailErr" />
              <span class="form-control-feedback" aria-hidden="true"></span>
              </div>
            </div>
            <div class="form-group">
            <div class="input-group">
            <p class="hiddenElement" id="passwordPlaceholder"><spring:message code="password"/></p>
            <div class="input-group-addon"><span id="addon3"><i class="fa fa-lock"></i></span></div>
              <form:input path="psw" type="password" class="form-control inputField psw-field" id="psw" placeholder=""/>
              <div class="input-group-addon" id="show_psw"><span class="glyphicon glyphicon-eye-open"></span></div>
              <input type="text" class="errorField" id="pswErr" />
              <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            </div>
            
            <div class="form-group">
            <div class="input-group">
            <p class="hiddenElement" id="psw2Placeholder"><spring:message code="confirmpass"/></p>
            <div class="input-group-addon"><span id="addon4"><i class="fa fa-lock"></i></span></div>
              <input type="password" class="form-control inputField psw2-field" id="psw2" placeholder=""/>
              <input type="text" class="errorField" id="psw2Err" />
              <span class="form-control-feedback" aria-hidden="true"></span>
            </div>
            </div>
          </form:form>
        </div>
        <div class="modal-footer">
          <input type="hidden" id="formStatus"> <!-- elemento candidato para eliminar -->
          <button type="button" class="btn btn-cancel pull-left cancel" id="signup_cancel_btn"><span class="glyphicon glyphicon-remove">
          </span><b> <spring:message code="cancel"/></b></button>
          <button type="button" class="btn btn-default btn-success btn-inline" id="signUp">
          <span><i class="material-icons" style="font-size: 17px;">cloud_upload</i></span><b> <spring:message code="signup"/></b> </button>
          <br/>
          <br/>
          <p style="display: inline;"><spring:message code="fbSignup"/> </p> 
          <fb:login-button scope="public_profile,email" onlogin="checkLoginState(false, true);"></fb:login-button>
          <br/>
          <p><spring:message code="member"/> <a id="goToLogin" class="custom-a"><spring:message code="login"/></a></p>
        </div>
      </div>
    </div>
  </div> 
     <!-- signUp modal -->
     
     <!-- Login modal -->
  <div id="loginModal" class="hiddenElement">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="cancel close" data-dismiss="modal">&times;</button><i class="fa fa-sign-in"></i> <spring:message code="login"/>
        </div>
        <div class="modal-body">
        
        <div id="loginErrDiv">
        
        </div>
        
          <form action="j_spring_security_check" method="post" name="loginForm" id="loginForm">
            <div class="form-group">
              <div class="input-group">
      <div class="input-group-addon"><span><i class="fa fa-user"></i></span></div>
      <p class="hiddenElement" id="usrNamePlaceholder"><spring:message code="username"/></p>
      <input name="j_username" type="text" class="form-control" id="usrNameLogin" placeholder=""/>
    </div>
            </div>
           <div class="form-group">
            <div class="input-group">
            <div class="input-group-addon"><span><i class="fa fa-lock""></i></span></div>
            <p class="hiddenElement" id="passwordPlaceholder"><spring:message code="password"/></p>
              <input name="j_password" type="password" class="form-control" id="pswLogin" placeholder=""/>
              <div class="input-group-addon" id="show_psw"><span class="glyphicon glyphicon-eye-open"></span></div>
            </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
           <button type="button" id="loginCancelBtn" class="btn btn-cancel pull-left cancel" data-dismiss="modal">
           <span class="glyphicon glyphicon-remove"></span> <b><spring:message code="cancel"/></b></button>
          <button name="submit" value="submit" type="button" class="btn btn-default btn-success btn-inline" id="loginSubmit">
          <i class="fa fa-sign-in"></i> <b><spring:message code="login"/></b></button>
          <br>
          <br>
          <p style="display: inline;"><spring:message code="fbSignup"/> </p> 
          <fb:login-button scope="public_profile,email" onlogin="checkLoginState(false);"></fb:login-button>
          <p><spring:message code="notmember"/> <a id="goToSignUp" class="custom-a">
          <spring:message code="signup"/></a></p>
          <p><spring:message code="forgot"/> <a class="custom-a" id="recoverPsw"><spring:message code="password"/>
          <span class="glyphicon glyphicon-question-sign"></a></p>
        </div>
      </div>
    </div>
  </div> 
     <!-- Login modal -->
  
  <!-- done modal -->
  <div id="doneModal" class="hiddenElement">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="cancel close"  data-dismiss="modal">&times;</button>
          <i class="fa fa-thumbs-o-up"></i> <spring:message code="signupsuccessful"/>
        </div>
        <div class="modal-body">
          <h1> <spring:message code="doneHeadTxt"/> </h1>
          <h3> <spring:message code="doneBodyTxt"/> </h3>
          <h4> <spring:message code="doneFooterTxt"/> </h4>
          
        </div>
        <div class="modal-footer">
           <a id="goToLogin" class="custom-a"><spring:message code="login"/></a>
        </div>
      </div>
    </div>
  </div>
  <!-- done modal --> 
  
    <!-- recoverPasswor modal -->
  <div id="recoverPasswordModal" class="hiddenElement">

    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="cancel close" data-dismiss="modal">&times;</button>
         <i class="fa fa-refresh"></i> <spring:message code="recoverpassword"/>
        </div>
        <div class="modal-body" id="modal-body">
          <form  id="recoverForm">
            <div class="form-group">
              <div class="input-group">
              <p class="hiddenElement" id="emaiForRecoverlPlaceholder"><spring:message code="emaiForRecoverlPlaceholder"/></p>
      <div class="input-group-addon"><span id="addon1"><i class="fa fa-envelope"></i></span></div>
      <input type="text" class="form-control inputField email-field email-field-recover" id="emailForRecover" name="emailForRecover" placeholder=""/>
      <input type="text" class="errorField" id="emailForRecoverErr" />
      <span class="form-control-feedback" aria-hidden="true"></span>
    </div>
            </div>

          </form>
        </div>
        <p id="recoverSuccsessMsg" class="hiddenElement"><spring:message code="recoverSuccsessMsg"/></p>
        <p id="recoverFailMsg" class="hiddenElement"><spring:message code="recoverFailMsg"/></p>
        <div class="modal-footer">
          <button type="button" class="btn btn-cancel pull-left cancel" id="recoverCancelBtn"><span class="glyphicon glyphicon-remove">
          </span><b> <spring:message code="cancel"/></b></button>
          <button type="button" class="btn btn-default btn-success btn-inline" id="recoverSaveBtn"><i class="fa fa-refresh"></i><b> <spring:message code="recoverpassword"/></b> </button>
          
        </div>
      </div>
    </div>
  </div> 
 <!-- recoverPasswor modal -->

	</div>
	
	<div class="modal fade container" id="about_us_modal_div" role="dialog">
	<!-- el contenido de este div se autocompleta mediante un script desde home.js -->
	</div>
	
	<div class="modal fade container" id="set_new_password_modal_div" role="dialog">
	<!-- el contenido de este div se autocompleta mediante un script desde home.js -->
	</div>
	
	<form action="j_spring_security_check" method="post" name="fbLoginForm" id="fbLoginForm" class="hiddenElement">
              <input name="j_username" type="text" id="fbUsrNameLogin"/>
              <input name="j_password" type="password" id="fbPswLogin"/>
          </form>
	
</body>

<script src="assets/js/jquery-1.12.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validate.js"></script>
<script src="https://www.promisejs.org/polyfills/promise-7.0.4.min.js"></script>
<script src="assets/js/home.js"></script>

</html>