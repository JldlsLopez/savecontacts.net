<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>

<title>SaveContacts.net</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/custombootstrap.css">
<link rel="stylesheet" href="assets/css/home.css">
<link rel="stylesheet" href="assets/css/loading-modal.css">
<link rel="stylesheet" href="assets/css/animateText.css">
<link rel="icon" href="assets/ico/favico.ico">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nova+Flat|Gloria+Hallelujah|Indie+Flower|Exo|Amita|Dosis|Cabin+Sketch|Roboto+Slab" rel="stylesheet">
</head>

<body>

<p id="hidenRequestJsp" class="hiddenElement">${requestJsp}</p>

<div id="body">
<div class="modal fade" id="loading_modal" role="dialog">
   <div class="bubblingG">
	<span id="bubblingG_1">
	</span>
	<span id="bubblingG_2">
	</span>
	<span id="bubblingG_3">
	</span>
</div>
</div>
<p id="hiddenUserName" style="display:none"><sec:authentication property="name"/></p>

	<!-- Barra de menus -->
	<nav class="navbar navbar-inverse ">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#myNavbar">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">SaveContacts.net</a>
			</div>
			<form id="user_logout_form" action="j_spring_security_logout" class="hiddenElement"></form>
			<div class="collapse navbar-collapse" id="myNavbar">
				<ul class="nav navbar-nav">
					<li><a id="btnAboutUsModal" class="custom-a"><i class="fa fa-group"></i> <spring:message code="AboutUs"/></a></li>
					
					<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">
					  <i class="material-icons" style="font-size: 14px;">contacts</i> <spring:message code="ContactUs"/> <span class="caret"></span></a>
						<ul class="dropdown-menu"> 
						<li> <pre><i class="fa fa-whatsapp" style="font-size:14px; margin-left:5px; color:green;"></i> Whatsapp: 829-872-4331</pre></li>
						<li class="divider"></li>
						<li> <pre><i class="fa fa-envelope-o" style="font-size:14px; margin-left:5px; color:red;"></i> Email: jorgeluisdelossantoslopez@gmail.com</pre></li>
						<li class="divider"></li>
						<li> <pre><i class="fa fa-bitbucket" style="font-size:14px; margin-left:5px;"></i> Bitbucket: <a class="custom-a" href="https://JldlsLopez@bitbucket.org" target="_blank">https://JldlsLopez@bitbucket.org</a></pre></li>
					   </ul>
					 </li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><i class="fa fa-user"></i> <sec:authentication property="name"/> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a class="custom-a" onclick="checkLoginState2();"> 
							<i class="fa fa-sign-out" style="top: 3px; font-size: 18px;"></i> <spring:message code="logout"/></a></li>
							<li class="divider"></li>
						</ul></li>

					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"> <span
							class="glyphicon glyphicon-globe"></span> <spring:message
								code="language"/> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="?language=en"><spring:message
										code="english"/></a></li>
										<li class="divider"></li>
							<li><a href="?language=es"><spring:message
										code="spanish"/></a></li>
						</ul></li>

				</ul>
			</div>
		</div>
	</nav>
	<div class="container"></div>
	<!-- Final barra de menus -->

	<div class="jumbotron text-center">
		<div class="frame">
			  <h1 class="title">SaveContacts</h1>
			    <ul id="ul">
            <li><h1 class="title">.net</h1></li>
	        <li><h1 class="title" style="opacity: 0.5;">.<span class="glyphicon glyphicon-phone"></span></h1></li>
		    <li><h1 class="title" style="opacity: 0.5;">.<span class="glyphicon glyphicon-cloud-upload"></span></h1></li>
			    </ul>
		</div>
		<p class="subTittle"><spring:message code="greeting"/> <sec:authentication property="name"/></p>
	</div>
	<p class="hiddenElement" id="searchPlaceholder"><spring:message code="searchMsg"/></p>
	<ul class="nav nav-tabs">
		<li id="li_tab_1" class="active"><a data-toggle="tab" href="#tab1"><b><spring:message code="newContact"/></b></a></li>
		<li id="li_tab_2"><a id="a_tab_2" data-toggle="tab" href="#tab2"><span class="badge" id="tableSize"></span> <b><spring:message code="contacts"/></b></a></li>
		<li> <input name="search" type="text" id="search" placeholder=""/> </li>
	</ul>
	<!-- nota: espesificar el height en % -->
	<div class="tab-content">
		<div id="tab1" class="tab-pane fade in active"
			style="overflow-y: auto; overflow-x: hidden; height: 250px;">
			<input type="hidden" name="stage" id="stage" value="0">
			<br/>
			<div class="form-content" id="form-content">

                         <div id="addNewContact">
							<button type="button" id="addContact" data-toggle="tooltip" title="Create New Contact" data-placement="bottom"><i class="material-icons">person_add</i></button>
						</div>
				<!-- Formulario de contacto -->
				<div id="form1" class="hiddenElement">
					<form id="contactForm">
					<input type="hidden" name="id" value="0"/>
						<div class="form-group">
                            <p class="hiddenElement" id="namePlaceholder"><spring:message code="name"/></p>
							<input type="text" class="form-control"
								name = "name" id="name" placeholder = "" />
						</div>
						<div class="form-group">
						<p class="hiddenElement" id="surnamePlaceholder"><spring:message code="surname"/></p>
							<input type="text" class="form-control" name="surname" id="surname" placeholder=""  />
						</div>
						<div>
							<label class="radio-inline"><input name="gender" type="radio"
									value="male" /> <spring:message code="male"/></label> 
									<label class="radio-inline"><input name="gender" type="radio"
									 value="female" /> <spring:message code="female"/></label>
				 		</div>
                         <br/>
						<div>
						<button type="button" class="btn btn-cancel btn-cancel-contact cancel">
                        <span class="glyphicon glyphicon-remove"></span> <b><spring:message code="cancel"/></b>
                        </button>
							<button type="button" id="contact_next"
								class="btn btn-default btn-success"><spring:message code="next"/>
								<span class="glyphicon glyphicon-chevron-right"></span>
							</button>
						</div>

					</form>
				</div>
				<!-- Fin formulario de Contacto -->

				<!-- Formulario de Numero -->
				<div id="form2" class="hiddenElement">
					<form id="numberForm">
					<fieldset id="fieldset1">
					    <div class="form-group">
					    <p class="hiddenElement" id="numberPlaceholder"><spring:message code="number"/></p>
					    <input type="hidden" name="id" value="0"/>
						<input type="tel" name="number" class="form-control only-number" placeholder="" id="nInput0" />
						</div>
						</fieldset>
					</form>
					
					<div class="form-group" id="divAddNumber">
							<button type="button" id="addNumber">
								<span class="glyphicon glyphicon-plus" id="plusSpan"></span>
							</button>
						</div>
						<button type="button" id="back_to_contact"
							class="btn btn-default btn-success">
							<span class="glyphicon glyphicon-chevron-left"></span><spring:message code="back"/>
						</button>
						<button type="button" class="btn btn-cancel btn-cancel-contact cancel">
                        <span class="glyphicon glyphicon-remove"></span> <b><spring:message code="cancel"/></b>
                        </button>
						<button type="button" id="number_next"
							class="btn btn-default btn-success"><spring:message code="next"/>
							<span class="glyphicon glyphicon-chevron-right"></span>
						</button>
				</div>
				<!-- Fin formulario de Numero -->
				
				<!-- Formulario de Direccion -->
				<div id="form3" class="hiddenElement">
					<form id="addressForm">
					<input name="id" type="hidden" value="0"/>
						<div class="form-group">
						<p class="hiddenElement" id="streetPlaceholder"><spring:message code="street"/></p>
							<input name="street" type="text" class="form-control"
								id="street" placeholder="" />
						</div>
						<div class="form-group">
						<p class="hiddenElement" id="cityPlaceholder"><spring:message code="city"/></p>
							<input name="city" type="text" class="form-control"
								id="city" placeholder="" />
						</div>
						<div class="form-group">
						<p class="hiddenElement" id="statePlaceholder"><spring:message code="state"/></p>
							<input name="state" type="text" class="form-control"
								id="state" placeholder="" />
						</div>
						<div class="form-group">
						<p class="hiddenElement" id="zypCodePlaceholder"><spring:message code="zypcode"/></p>
							<input name="zypCode" type="text" class="form-control"
								id="zypCode" placeholder="" />
						</div>
						<button type="button" id="back_to_number"
							class="btn btn-default btn-success">
							<span class="glyphicon glyphicon-chevron-left"></span><spring:message code="back"/>
						</button>
						<button type="button" class="btn btn-cancel btn-cancel-contact cancel">
                        <span class="glyphicon glyphicon-remove"></span> <b><spring:message code="cancel"/></b>
                        </button>
						<button type="button" id="address_next"
							class="btn btn-default btn-success"><spring:message code="next"/>
							<span class="glyphicon glyphicon-chevron-right"></span>
						</button>
					</form>
				</div>
				<!-- Fin formulario de Direccion -->
				
				<!-- Formulario de Mail -->
				<div id="form4" class="hiddenElement">
					<form id="mailForm">
					<fieldset id="fieldset2">
					    <div class="form-group">
					    <input name="id" type="hidden" value="0"/>
					    <p class="hiddenElement" id="emailPlaceholder"><spring:message code="email"/></p>
						<input name="mail" type="email" class="form-control " placeholder="E-mail address" id="mInput0" />
						</div>
						</fieldset>
					</form>
					
					<div class="form-group" id="divAddMail">
							<button type="button" id="addMail">
								<span class="glyphicon glyphicon-plus"></span>
							</button>
						</div>
						<button type="button" id="back_to_address"
							class="btn btn-default btn-success">
							<span class="glyphicon glyphicon-chevron-left"></span><spring:message code="back"/> 
						</button>
						<button type="button" class="btn btn-cancel btn-cancel-contact cancel">
                        <span class="glyphicon glyphicon-remove"></span> <b><spring:message code="cancel"/></b>
                        </button>
						<button type="button" id="submit_contact"
							class="btn btn-default btn-success"><spring:message code="save"/> 
							<span class="glyphicon glyphicon-cloud-upload"> </span>
						</button>
				</div>
				<!-- Fin formulario de Mail -->
			</div>
		</div>
		<div id="tab2" class="tab-pane fade" style="overflow-y: auto; overflow-x: auto; height: 250px;">
		<br/>
			<table
				class="table table-striped table-bordered table-hover table-condensed" id="contactTable"  style="display: none">
				<thead>
					<tr id="table_head">
						<th><spring:message code="name"/></th><th><spring:message code="surname"/></th>
						<th><spring:message code="gender"/></th><th><spring:message code="number"/></th>
						<th><spring:message code="address"/></th><th><spring:message code="email"/></th> 
						<th colspan="2"><spring:message code="options"/></th>
					</tr>
				</thead> 
				<tbody id="table_body">
                   
				</tbody>
			</table>
		</div>
	</div>
</div>
   <div class="modal fade" id="aboutUsModal" role="dialog">
	
	</div>
</body>

<script src="assets/js/jquery-1.12.2.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/user.js"></script>
<script src="assets/js/validate.js"></script>
<script src="assets/js/facebook-login.js"></script>
<script>
	$(document).on('ready',function() {
		$('#btnAboutUsModal').click(function() {
             $.ajax({
      type: "POST",
		url : "AboutUs.html",
		success : function(response) {
			var html = response;
			modalHtml = html.substr(html.indexOf('<!--landmark-->'), html.indexOf('<!--LANDMARK-->'));
			document.getElementById('aboutUsModal').innerHTML = modalHtml;
			$("#aboutUsModal").modal({backdrop: "static"});
		}});
 });
	});
	
	
	
</script>
</html>