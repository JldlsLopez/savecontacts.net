
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!-- La estructura de este jsp esta pensada solo para que el codigo html de la ventana modal sirva de recurso para otros jsp's, como Home.jsp, etc. -->
<html lang="en">
<body>
<!-- Modal -->
  <div class="modal fade" id="aboutUsModal" role="dialog">
  <!--landmark-->
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-group"></i> <spring:message code="AboutUs"/></h4>
        </div>
        <div class="modal-body" id="modal_body">
          <p id="p_about"><spring:message code="aboutTxt"/></p>
          <div class="table-responsive">
			  <table class="table table-striped" id="about_table">
			    <thead>
		          <tr>
		          <th>FRONT-END</th>
		          <th>BACK-END</th>
		          </tr>
		          </thead>
		          <tbody>
		          <tr>
		          <td><a href="http://www.w3schools.com/html/html_intro.asp" target="_blank">HTML</a> - 
		          <a href="http://www.w3schools.com/html/html5_intro.asp" target="_blank">HTML5</a></td>
		          <td><a href="https://www.java.com/es/about/whatis_java.jsp" target="_blank">Java</a></td>
		          </tr>
		          <tr>
		          <td><a href="http://www.w3schools.com/css/css_intro.asp" target="_blank">CSS</a> - <a href="http://www.w3schools.com/css/css3_intro.asp" target="_blank">CSS3</a></td>
		          <td><a href="http://hibernate.org/orm/" target="_blank">Hibernate ORM</a></td>
		          </tr>
		          <tr>
		          <td><a href="http://www.w3schools.com/js/" target="_blank">Javascript</a> - <a href="https://jquery.com/" target="_blank">Jquery framework</a></td>
		          <td><a href="https://maven.apache.org/what-is-maven.html" target="_blank">Maven</a></td>
		          </tr>
		          <tr>
		          <td><a href="http://getbootstrap.com/" target="_blank">BootStrap framework</a></td>
		          <td><a href="https://www.mysql.com/about/" target="_blank">MySql</a></td>
		          </tr>
		          <tr>
		          <td><a href="http://www.w3schools.com/xml/ajax_intro.asp" target="_blank">Ajax</a></td>
		          <td><a href="http://tomcat.apache.org/" target="_blank">Apache Tomcat</a></td>
		          </tr>
		          <tr>
		          <td><a href="https://developers.facebook.com/docs/graph-api/overview/" target="_blank">Facebook API Graph</a></td>
		          <td><a href="https://en.wikipedia.org/wiki/Gson" target="_blank">Gson</a></td>
		          </tr>
		          <tr>
		          <th colspan="2">Others</th>
		          </tr>
		          <tr>
		          <td colspan="2"><a href="https://spring.io/tools" target="_blank">Spring Tool Suite IDE</a></td>
		          </tr>
		          <tr>
		          <td colspan="2"><a href="http://projects.spring.io/spring-framework/" target="_blank">Spring framework</a></td>
		          </tr>
		          <tr>
		          <td colspan="2"><a href="https://aws.amazon.com/" target="_blank">Amazon AWS</a></td>
		          </tr>
		          </tbody>
			  </table>
			</div>
        </div>
      </div>
    </div>
    <!--LANDMARK-->
    </div>
</body>
</html>