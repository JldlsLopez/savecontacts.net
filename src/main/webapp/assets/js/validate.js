//variables de ambito global
var string1 = 'waiting';
var showModal = false;
var tmpHtml = '';

$(document).ready(function(){
	var status = 0;

	function validateInput(elementId, available1, available2){

		elementId = '#'+elementId;
		var elementErrId = elementId+'Err';
		var value = $(elementId).val();

		if(value == null || value == ""){

			status = -1;
			$(elementErrId).val(document.getElementById('signupErrMsg1').innerHTML);
			toggleInput(elementId, elementErrId);

		}else if(/\s{1,}/.test(value)){

			status = -1;
			$(elementErrId).val(document.getElementById('signupErrMsg2').innerHTML);
			toggleInput(elementId, elementErrId);
		}else if(/\W{1,}/.test(value) && $(elementId).hasClass('usr-name-field')){

			status = -1;
			$(elementErrId).val(document.getElementById('signupErrMsg3').innerHTML);
			toggleInput(elementId, elementErrId);

		}else{

			if($(elementId).hasClass('usr-name-field')){// validate username
				
							if(available1 === true){
								if(value.length >=6){
									if(/[a-z]{1,}|[0-9]{1,}[a-z]{1,}/i.test(value)){
										
										status = 1;
										$(elementId).addClass('field_success');
									}
									else{

										status = -1;
										
										$(elementErrId).val(document.getElementById('signupErrMsg4').innerHTML);
										toggleInput(elementId, elementErrId);
									}
								}else{
									status = -1;
									
									$(elementErrId).val(document.getElementById('signupErrMsg5').innerHTML);
									toggleInput(elementId, elementErrId);
								}
							}else{
								status = -1;
								
								$(elementErrId).val(document.getElementById('signupErrMsg6').innerHTML);
								toggleInput(elementId, elementErrId);
							}

			}else if($(elementId).hasClass('email-field')){//validate email
				
							if(available2 === true && !$(elementId).hasClass('email-field-recover')){
								if(/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/.test(value)){
									status = 1;
									
									$(elementId).addClass('field_success');
								}else{
									    status = -1;
									    
										$(elementErrId).val(document.getElementById('signupErrMsg7').innerHTML);
										toggleInput(elementId, elementErrId);
								}	

							}else if(available2 ===  false && !$(elementId).hasClass('email-field-recover') && 
									/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/.test(value)){
								status = -1;
								
								$(elementErrId).val(document.getElementById('signupErrMsg8').innerHTML);
								toggleInput(elementId, elementErrId);
							}

							//recover
							if($(elementId).hasClass('email-field-recover')){
								
								if(/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/.test(value)){
									if(available2 ===  false){
										status = 1;
										
										$(elementId).addClass('field_success');
									}else{
										status = -1;
										
										$(elementErrId).val(document.getElementById('recoverErrMsg').innerHTML);
										toggleInput(elementId, elementErrId);
									}
								}else{
									status = -1;
									
									$(elementErrId).val(document.getElementById('signupErrMsg7').innerHTML);
									toggleInput(elementId, elementErrId);
								}
								
							}
					

			}else if($(elementId).hasClass('psw-field')){//validate password
				if(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value)){
					status = 1;
					$(elementId).addClass('field_success');
				}else{
					$('#show_psw').fadeOut(800);
					if(value.length < 8 || !/[A-Z]/.test(value) || !/[a-z]/.test(value) || !/[0-9]/.test(value) || !/[\W]/.test(value)){
						status = -1;
						$(elementErrId).val(document.getElementById('signupErrMsg9').innerHTML);
						toggleInput(elementId, elementErrId);
					}
				}
			}else if($(elementId).hasClass('psw2-field')){
				var psw = $('#psw').val();
				var psw2 = $('#psw2').val();
				if(psw == psw2){
					status = 1;
					$('#psw2').addClass('field_success');
				}else{

					if(psw == ''){
						status = -1;
						$(elementErrId).val(document.getElementById('signupErrMsg10').innerHTML);
						toggleInput(elementId, elementErrId);
					}else{

						status = -1;
						$(elementErrId).val(document.getElementById('signupErrMsg11').innerHTML);
						toggleInput(elementId, elementErrId);
					}
				}
			}

		}
	}

	function toggleInput(elementId, elementErrId){

		$(elementId).fadeOut(800, function(){
			$(elementErrId).fadeIn(800);
			$(elementErrId).addClass('form-control');
			$(elementErrId).parents('div.form-group').addClass("has-error");
			$(elementErrId).parents('div.form-group').addClass("has-feedback");
			$(elementErrId).parents('div.form-group').find('span.form-control-feedback').addClass('glyphicon glyphicon-remove');
		});
	}

	$('.errorField').attr('readonly', 'readonly');
	var addon;
	$(document).on('focusin', '.errorField', function() {
		$('#signUp').removeAttr('disabled');
		var elementErrId = $(this).attr('id');
		var elementId = elementErrId.substr(0, elementErrId.length - 3);
		$('#'+elementErrId).fadeOut(200, function(){
			$('#'+elementId).fadeIn(800);
			if(elementId == 'psw' && $('#'+elementId).val().length > 0){
				$('#show_psw').fadeIn(800);
			}
			addon = $('#'+elementId).prev('div').children('span').attr('id');
			$('#'+addon).click();
			$('#'+elementId).focus();
		});
	});

	$(document).on('keyup', '#usrNameLogin', function() {
		$('#loginError').fadeOut(2000);

	});
	$(document).on('keyup', '#psw', function() {
		var psw_val = $('#psw').val();
		if(psw_val !="" && psw_val.length > 0){
			$('#show_psw').fadeIn(800);
			$('#show_psw').css('display', 'table-cell');
		}else if(psw_val.length == 0){

			$('#show_psw').fadeOut(800);
		}
	});

	$(document).on('click', '#signUp', function() {
		
		var formStatus = 0;
		var pswStatus = 0;
		
		$.when($.ajax("check_username_availability.json?userName=" + $("#userName").val()), 
			   $.ajax("check_mail_availability.json?mail=" + $("#mail").val())).done(function(response1, response2) {
				   
		     var available1 = response1[0];
		     var available2 = response2[0];
		     
		     validateInput("usrName", available1 , available2);
		     formStatus += status;
		     validateInput("mail", available1 , available2);
		     formStatus += status;
			 validateInput("psw", available1 , available2);
			 pswStatus = status;
	    	 formStatus += status;
	    	 validateInput("psw2", available1 , available2);
		     formStatus += status;
		     if(formStatus == 4){
	    		 $("#modal_div").addClass('loading');
					$('#modal_div').modal('hide');
					
	 		}else{
	 			if(pswStatus == -1){$('#show_psw').fadeOut(800);}
	 			$('#signUp').prop('disabled', 'true');
	 			
	 		}
		     status = 0;
		});
		    	
	});
	
	$(document).on('click', '.form-group', function() {
		if($(this).attr('class') == 'form-group has-error has-feedback'){
			$(this).removeClass("has-error");
			$(this).removeClass("has-feedback");
			$(this).find('span.form-control-feedback').removeClass('glyphicon glyphicon-remove');
		}
	});
	
	function restartInputs() {
		
		$('#modal_div').modal('hide');
		$('#modal_div').on('hidden.bs.modal', function() {
			$(".form-group").removeClass("has-error");
			$(".form-group").removeClass("has-feedback");
			$('.form-control-feedback').removeClass('glyphicon glyphicon-remove');
			$('.errorField').fadeOut(800, function(){
				$('.inputField').fadeIn(800);
			});
		});
	}
	
	$(document).on('click', '#signup_cancel_btn', function() {
		$('#signUp').removeAttr('disabled');
		restartInputs();
		status = 0;
	});
	
	$(document).on('click', '#recoverCancelBtn', function() {
		restartInputs();
		
});

	$(document).on('click', '.form-control', function(){

		$(this).removeClass('field_success');
		$('#signUp').removeAttr('disabled');
	});

	$(document).on('click', '#show_psw', function(){//oculta o muestra la contrase?a
		if($(this).children('span').attr('class') == 'glyphicon glyphicon-eye-open'){
			$('#psw').attr('type','text');
			$('#psw2').attr('type','text');
			document.getElementById("addon3").innerHTML = '<i class="fa fa-unlock-alt" style="font-size: 17px;"></i>';
			document.getElementById("addon4").innerHTML = '<i class="fa fa-unlock-alt" style="font-size: 17px;"></i>';
			$(this).children('span').attr('class', 'glyphicon glyphicon-eye-close');

		}else if($(this).children('span').attr('class') == 'glyphicon glyphicon-eye-close'){
			$('#psw').attr('type','password');
			$('#psw2').attr('type','password');
			document.getElementById("addon3").innerHTML = '<i class="fa fa-lock" style="font-size: 17px;"></i>';
			document.getElementById("addon4").innerHTML = '<i class="fa fa-lock" style="font-size: 17px;"></i>';
			$(this).children('span').attr('class', 'glyphicon glyphicon-eye-open');
		}
	});

	$(document).on( "keydown", '.only-number', function( event ) {
		var value = event.which;
		if(value == 8 || value >= 48 && value <= 57 || value >= 96 && value <= 105){

		}else{
			return false;
		}
	});
	
	//script for recover password
	$(document).on('click', '#recoverSaveBtn', function() {
		
		var emailForRecover = $('#emailForRecover').val();
		
		$.when($.ajax("check_mail_availability.json?mail=" + emailForRecover)).done(function(response2) {
			
			validateInput("emailForRecover", false, response2);
			
				if(status == 1){
					
					$('#recoverSaveBtn').css("display", "none");
					$('#recoverCancelBtn').css("display", "none");
					
					showModal = true;
					$('#modal_div').modal('hide');
					$('#modal_div').on('hidden.bs.modal', function() {
						if(showModal){
							
							$("#loading_modal").modal({backdrop: "static"});
						}
					});
					
					$.when($.ajax({
						type: 'POST',
						url: 'request_reset_login_user_password.html',
						data: 'emailForRecover='+emailForRecover
						})).done(function(response3) {
							
								var index = response3.indexOf(',');
								
								string1 = response3.substr(0, index);
								var string2 = response3.substr(index+1, response3.length);
								tmpHtml = document.getElementById('modal-body').innerHTML;
									if(string1 == "success"){
										
					                    document.getElementById('modal-body').innerHTML = 
					                    	'<h3 class="hiddenElement" id="recoverSuccsessMsgOn">'+ 
					                    	document.getElementById('recoverSuccsessMsg').innerHTML + ' ' + string2 + '</h3>';
					                    $('#recoverSuccsessMsgOn').css('display', 'block');
					                    
									}else if(string1 == "fail"){
										
										document.getElementById('modal-body').innerHTML = 
					                    	'<h3 class="hiddenElement" id="recoverFailMsgOn">'+ 
					                    	document.getElementById('recoverFailMsg').innerHTML + '</h3>';
					                    $('#recoverFailMsgOn').css('display', 'block');
									}
									
									$("#loading_modal").modal('hide');
							
						});
					
					
				}
			
		});
		
	});
	
	$(document).on('click', '#newPswSaveBtn', function() {
		
		     var newPsw = $('#new_psw').val();
			 var userKey = $('#userKeyPsw').val();
			 validateInput("new_psw", false, false);
   		 if(status == 1){
   			 
   			$.when($.ajax({
				type: "POST",
				url: "set_new_password.html",
			    data: "psw="+newPsw + "&userMail="+userKey
			})).done(function(response4) {
				if(response4 == "success"){
						$('#newPswSaveBtn').css("display", "none");
						$('#newPswCancelBtn').css("display", "none");
						
	                    document.getElementById('new_psw_modal_body').innerHTML = 
	                    	'<h3 class="hiddenElement" id="passwordChangedOn">'+ document.getElementById('passwordChanged').innerHTML +'</h3>';
	                    $('#xbtnCancel').attr('data-dismiss', '');
	                    $('#xbtnHref').attr('href', 'Home.html');
	                    $('#passwordChangedOn').fadeIn(1000);
					}
			});
 			 
   		 }
		
			
	});

});

