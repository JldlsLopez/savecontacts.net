function showLoadingModal() {
	$("#loading_modal").modal({backdrop: "static"});
}

var requestJsp = $('#hidenRequestJsp').text();
if(requestJsp == "home" || requestJsp == "login"){
	showLoadingModal();
}
	$(document).on('ready',function() {
		//Done modal
		if(requestJsp == "done"){
			document.getElementById("loginForm").reset();
			document.getElementById("signupForm").reset();
			$("#doneModal").slideDown();
			
			$("#modal_div").modal({backdrop: "static"});
			
		}
		
		//AboutUs modal
         $(document).on('click', '#btnAboutUsModal', function() {
     	               var promise = $.ajax({
     		            type: "POST",
						url : "AboutUs.html"
						});
     	               
     	               promise.then(
     	            		  function(response) {
     								var html = response;
     								modalHtml = html.substr(html.indexOf('<!--landmark-->'), html.indexOf('<!--LANDMARK-->'));
     								document.getElementById('about_us_modal_div').innerHTML = modalHtml;
     								$("#about_us_modal_div").modal({backdrop: "static"});
     							}	   
     	               );
     	                       
                    });
		//loging
		$(document).on('click', '#loginHome', function(){
			$("#modal_div").addClass('loginHome');
			
						
						$('#usrNameLogin').attr('placeholder', document.getElementById('usrNamePlaceholder').innerHTML);
						$('#pswLogin').attr('placeholder', document.getElementById('passwordPlaceholder').innerHTML);
						
						document.getElementById('loginErrDiv').innerHTML = document.getElementById('homeErrDiv').innerHTML;
						document.getElementById('homeErrDiv').innerHTML = '';
						
						$("#signupModal").css('display', '');
						$("#doneModal").css('display', '');
						$("#recoverPasswordModal").css('display', '');
						
						document.getElementById("loginForm").reset();
						
						$("#modal_div").modal({backdrop: "static"});
						
              	    });
		
		$(document).on('click', '#goToSignUp', function() {
			$("#modal_div").addClass('goToSignUp');
			$("#modal_div").modal('hide');
	    });
		
		// set email for recover password
		$(document).on('click', '#recoverPsw', function() {
	
					$('#emailForRecover').attr('placeholder', document.getElementById('emaiForRecoverlPlaceholder').innerHTML);
					
					$("#loginModal").slideUp(function() {
						
						document.getElementById("recoverForm").reset();
						$("#recoverPasswordModal").slideDown(function() {
							$('#emailForRecover').focus();
						});
					});
					
	    });
		
		//set a new password
		if(requestJsp == 'setNewPasswordModal'){
			requestJsp = '';
			var promise = $.ajax({
	            type: "GET",
			    url : "set_new_password.html"
		        });
		
		promise.then(
				function(response) {
					var html = response;
					modalHtml = html.substr(html.indexOf('<!--landmark-->'), html.indexOf('<!--LANDMARK-->'));
					document.getElementById('set_new_password_modal_div').innerHTML = modalHtml;
					$('#userKeyPsw').val(document.getElementById('userKeyHome').innerHTML);
					$('#new_psw').attr('placeholder', document.getElementById('newPasswordlPlaceholder').innerHTML);
					$("#set_new_password_modal_div").modal({backdrop: "static"});
						
				});
		}
		
		//signUpHome
		$(document).on('click', '#signUpHome', function(){
			            
			            $("#modal_div").addClass('signUpHome');
			
						$('#usrName').attr('placeholder', document.getElementById('usrNamePlaceholder').innerHTML);
						$('#mail').attr('placeholder', document.getElementById('mailPlaceholder').innerHTML);
						$('#psw').attr('placeholder', document.getElementById('passwordPlaceholder').innerHTML);
						$('#psw2').attr('placeholder', document.getElementById('psw2Placeholder').innerHTML);
						
						$("#loginModal").css('display', '');
						$("#doneModal").css('display', '');
						
						$("#modal_div").modal({backdrop: "static"});
						
						$("#modal_div").on('shown.bs.modal', function() {
							if($("#modal_div").hasClass('signUpHome')){
								document.getElementById("signupForm").reset();
								$("#signupModal").slideDown(function() {
									$('#usrName').focus();
								  	$("#modal_div").removeClass('signUpHome');
								});
							}
							});
			
	    });
		
		$(document).on('click', '#goToLogin', function() {
			$("#modal_div").addClass('goToLogin');
			$("#modal_div").modal('hide');
	    });
		
		$("#modal_div").on('shown.bs.modal', function() {
			if($("#modal_div").hasClass('loginHome')){
				$("#loginModal").slideDown(function() {
					$('#usrNameLogin').focus();
				  	$("#modal_div").removeClass('loginHome');
				});
			}
			
			if(requestJsp == "login"){
				$('#usrNameLogin').focus();
				}
			
		});
		
		$("#set_new_password_modal_div").on('shown.bs.modal', function() {
			
			$('#new_psw').focus();
		});
		
		$("#modal_div").on('hidden.bs.modal', function() {
			
			$("#signupModal").css('display', '');
			$("#loginModal").css('display', '');
			$("#aboutUsModal").css('display', '');
			$("#doneModal").css('display', '');
			$("#recoverPasswordModal").css('display', '');
			
			if($("#modal_div").attr('class').includes('loading')){
				showLoadingModal();
				$("#modal_div").removeClass('loading');
				$('#signupForm').submit();
			} 
			
			if($("#modal_div").attr('class').includes('goToSignUp')){
				$("#modal_div").removeClass('goToSignUp');
				$('#signUpHome').click();
			} 
			
			if($("#modal_div").attr('class').includes('goToLogin')){
				$("#modal_div").removeClass('goToLogin');
				$('#loginHome').click();
			}
			
			if($("#modal_div").attr('class').includes('recoverPsw')){
				$("#modal_div").removeClass('recoverPsw');
			}
			
			if($("#modal_div").hasClass('waitingForFB')){
				    showLoadingModal();
			}
			
			if(string1 != 'waiting'){
				 document.getElementById('modal-body').innerHTML = tmpHtml;
				 $('#recoverSaveBtn').css("display", "inline-block");
					$('#recoverCancelBtn').css("display", "inline-block");
					$('#emailForRecover').removeClass('field_success');
			}
			

		});
		
		$("#loading_modal").on('hidden.bs.modal', function() {
			
			//login modal
			if(requestJsp == "login" && string1 == 'waiting'){
				$('#usrNameLogin').attr('placeholder', document.getElementById('usrNamePlaceholder').innerHTML);
				$('#pswLogin').attr('placeholder', document.getElementById('passwordPlaceholder').innerHTML);
				
				document.getElementById("loginForm").reset();
				
				document.getElementById('loginErrDiv').innerHTML = document.getElementById('homeErrDiv').innerHTML;
				document.getElementById('homeErrDiv').innerHTML = '';
				string1 = 'waiting';
				
				$("#loginModal").slideDown(function() {
					
					$("#modal_div").modal({backdrop: "static"});
				});
				
			}
			
			if(string1 != 'waiting'){
                	showModal = false;
                	$('#recoverPasswordModal').css('display', 'block');
                	$('#modal_div').modal({backdrop: "static"});
			}
		});
		
		$(document).on('click', '#loginSubmit', function() {
			
			$('#loginForm').submit();
			
		});
});