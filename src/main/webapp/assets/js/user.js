jQuery(document).ready(function($){
	
	var intId = 0, numberLength = 0, numberString, keyPresed, contactAsJson, form1AsJson, form2AsJson, form3AsJson, form4AsJson;
	$('[data-toggle="tooltip"]').tooltip();
	var currentUserName = document.getElementById('hiddenUserName').innerHTML;
	updateTab();
    var namePlaceholder, surnamePlaceholder, numberPlaceholder, streetPlaceholder, cityPlaceholder, 
    statePlaceholder, zypcodePlaceholder, emailPlaceholder, searchPlaceholder;
   
    namePlaceholder = $('#namePlaceholder').text();
    surnamePlaceholder = $('#surnamePlaceholder').text();
    numberPlaceholder = $('#numberPlaceholder').text();
    streetPlaceholder = $('#streetPlaceholder').text();
    cityPlaceholder = $('#cityPlaceholder').text();
    statePlaceholder = $('#statePlaceholder').text();
    zypcodePlaceholder = $('#zypCodePlaceholder').text();
    emailPlaceholder = $('#emailPlaceholder').text();
    searchPlaceholder = $('#searchPlaceholder').text();
    
    $('#name').attr('placeholder', namePlaceholder);
    $('#surname').attr('placeholder', surnamePlaceholder);
    $('#nInput0').attr('placeholder', numberPlaceholder);
    $('#street').attr('placeholder', streetPlaceholder);
    $('#city').attr('placeholder', cityPlaceholder);
    $('#state').attr('placeholder', statePlaceholder);
    $('#zypCode').attr('placeholder', zypcodePlaceholder);
    $('#mInput0').attr('placeholder', emailPlaceholder);
    $('#search').attr('placeholder', ' '+searchPlaceholder);
                    
	function getUserContacts() {

		var promise = $.ajax({
			type : "GET",
			url : "/SaveContacts/get_current_user.json",
			data : "username=" + currentUserName
		});
								
	promise.then(function(response) {
		
		var i = 0;
		var tmpContact;
		var contactsListSize = response.contactsList.length;

	do {
		if(contactsListSize > 0){

			tmpContact = response.contactsList[i];
			var option1, option2, option3, sex;
			option1 = tmpContact.gender;
			sex = 'male';
			if (option1 != 'undefined') {
				if (option1 == sex) {

					option2 = 'female';
				} else {

					option2 = 'male';
				}
				option3 = 'undefined';
			} else {
				option1 = tmpContact.gender;
				option2 = 'male';
				option3 = 'female';
			}
		document.getElementById('tableSize').innerHTML = contactsListSize;
		
		$('#table_body').html($('#table_body').html()
			+ '<tr id="contact'
			+ tmpContact.id
			+ '" class="hidden_tr">'
			+ '<td id="field_row'
			+ tmpContact.id
			+ '">'
			+'<div class="hiddenElement inputDiv">'
			+ '<input type="text" value="'
			+ tmpContact.name
			+ '" class="table_field field_row'
			+ tmpContact.id
			+ '" id="name'
			+ tmpContact.id
			+ '" placeholder="'+namePlaceholder+'" />'
			+'</div>'
			+ '<pre class="contact-info contactIndex">'+tmpContact.name+'</pre>'
			+ '</td>'
			+
	
			'<td>'
			+'<div class="hiddenElement inputDiv">'
			+ '<input type="text" value="'
			+ tmpContact.surname
			+ '" class="hiddenElement table_field field_row'
			+ tmpContact.id
			+ '" id="surname'
			+ tmpContact.id
			+ '" placeholder="'+surnamePlaceholder+'" />'
			+'</div>'
			+ '<pre class="contact-info contactIndex">'+tmpContact.surname+'</pre>'
			+ '</td>'
			+
	
			'<td>'
			+ '<div class="hiddenElement inputDiv select_div">'
			+ '<select class="genderSelect table_field field_row'
			+ tmpContact.id
			+ '" id="currentGender'
			+ tmpContact.id
			+ '">'
			+ '<option>'
			+ option1
			+ '</option>'
			+ '<option>'
			+ option2
			+ '</option>'
			+ '<option>'
			+ option3
			+ '</option>'
			+ '</select>'
			+ '</div>'
			+ '<pre class="contact-info">'+option1+'</pre>'
			+ '</td>'
			
			+ '<td class="tdNumber" id="tdNumberId'
			+ tmpContact.id
			+ '">'
			+
			'</td>'
			+
			'<td class="tdAddress" id="tdAddressId'
			+ tmpContact.id
			+ '">'
			+
			'</td>'
			+
	
			'<td class="tdEmail" id="tdEmailId'
			+ tmpContact.id
			+ '">'
			+
	
			'</td>'
			+
	
			'<td class="contact_update">'
			+
	
			'<pre id="contact_update'
			+ tmpContact.id
			+ '" class="glyphicon glyphicon-pencil"'
			+ 'data-toggle="tooltip" title="Edit contact" data-placement="bottom"></pre>'
			+
	
			'</td>'
			+
	
			'<td class="contact_remove">'
			+
	
			'<pre id="contact_remove'
			+ tmpContact.id
			+ '" class="glyphicon glyphicon-remove"'
			+ 'data-toggle="tooltip" title="Remove contact" data-placement="bottom"></pre>'
			+
	
			'</td>' +
	
			'</tr>');

    // number
	for (var j = 0; j < tmpContact.numberList.length; j++) {

		var tmpNumber = tmpContact.numberList[j];
		var number = tmpNumber.number;
		if (number == 'empty') {
			number = '';
		}

        document.getElementById('tdNumberId'
		+ tmpContact.id).innerHTML += '<div class="hiddenElement inputDiv">'
		+'<input type="tel" class="hiddenElement table_field only-number field_row'
		+ tmpContact.id
		+ '" id="number'
		+ tmpNumber.id
		+ '"'
		+ ' value="'
		+ number
		+ '" placeholder="'+numberPlaceholder+'" />'
		
		+ '</div>'
		+ '<pre class="contact-info">'+number+'</pre>';
      }

	// address
	for (var k = 0; k < tmpContact.addressList.length; k++) {

		var tmpAddress = tmpContact.addressList[k];
		var zypcode = tmpAddress.zypCode;
		if (zypcode == 'empty') {
			zypcode = '';
		}
        document.getElementById('tdAddressId'
		+ tmpContact.id).innerHTML += '<div class="hiddenElement inputDiv">'
		+ '<input type="text" class="hiddenElement table_field field_row'
		+ tmpContact.id
		+ '" id="street'
		+ tmpAddress.id
		+ '"'
		+ ' value="'
		+ tmpAddress.street
		+ '" placeholder="'+streetPlaceholder+'" />'
		
		+'</div>'
		+ '<pre class="contact-info">'+tmpAddress.street+'</pre>'
		
		+ '<div class="hiddenElement inputDiv">'
		+ '<input type="text" class="hiddenElement table_field field_row'
		+ tmpContact.id
		+ '" id="city'
		+ tmpAddress.id
		+ '"'
		+ ' value="'
		+ tmpAddress.city
		+ '" placeholder="'+cityPlaceholder+'" />'
		
		+ '</div>'
		+ '<pre class="contact-info">'+tmpAddress.city+'</pre>'
		
		+ '<div class="hiddenElement inputDiv">'
		+ '<input type="text" class="hiddenElement table_field field_row'
		+ tmpContact.id
		+ '" id="state'
		+ tmpAddress.id
		+ '"'
		+ ' value="'
		+ tmpAddress.state
		+ '" placeholder="'+statePlaceholder+'" />'
		
		+ '</div>'
		+ '<pre class="contact-info">'+tmpAddress.state+'</pre>'
		
		+ '<div class="hiddenElement inputDiv">'
		+ '<input type="text" class="hiddenElement table_field field_row'
		+ tmpContact.id
		+ '" id="zypCode'
		+ tmpAddress.id
		+ '"'
		+ ' value="'
		+ zypcode
		+ '" placeholder="'+zypcodePlaceholder+'" />'
		
		+ '</div>'
		+ '<pre class="contact-info">'+zypcode+'</pre>';
		}

	// email
	for (var l = 0; l < tmpContact.mailList.length; l++) {

		var tmpMail = tmpContact.mailList[l];
		var email = tmpMail.mail;
		if (email == 'empty') {
			email = '';
		}
        document.getElementById('tdEmailId'
		+ tmpContact.id).innerHTML += '<div class="hiddenElement inputDiv">'
		+ '<input type="email" class="hiddenElement table_field field_row'
		+ tmpContact.id
		+ '" id="mail'
		+ tmpMail.id
		+ '"'
		+ ' value="'
		+ email
		+ '" placeholder="'+emailPlaceholder+'" />'
		+ '</div>'
		+ '<pre class="contact-info">'+email+'</pre>';
		}

        i++;
										
	  }
  } while (i < contactsListSize);
	
		trimInputs();
		resizeInput();
									
		$('#contactTable').fadeIn(2000, function() {
			var html = document.getElementById('body').innerHTML;
	       	 html = html.replace(/#/g, '~');
	    		 $('#html').val(html);
		});
	});
  }
		getUserContacts();

	function formatNumber(select) {
		
		$(document).on('keyup',select,function(event) {
							
			keyPresed = event.which;
			numberString = document.activeElement.value;
			numberLength = numberString.length;

			if (select.includes('nInput')) {

				if (numberLength > 0) {
					$('#addNumber').css("display", "block");

				} else {
					$('#addNumber').css("display", "none");
				}
			}

			if (select.includes('nInput') || select.includes('number')) {

				if (numberLength == 3 && keyPresed != '8') {
					document.activeElement.value = numberString + '-';

				} else if (numberLength == 7 && keyPresed != '8') {
					document.activeElement.value = numberString + '-';
				}
			}
		});
	}

	function addMail() {

		$('#mInput' + intId).keyup(function() {
			keyPresed = event.which;
			mailString = document.activeElement.value;
			mailLength = mailString.length;
			if (mailLength > 0) {
				$('#addMail').css("display", "block");

			} else {
				$('#addNumber').css("display", "none");
			}

		});
	}

		$('#addContact').click(function() {
			$('#addNewContact').fadeOut(700, function() {
				$("#form1").fadeIn(700);
				$('#name').focus();
			});

		});
					
	 $(document).on('click','#addNumber',function() {
		 
			intId++;
			var fieldWrapper = $("<div class=\"fieldwrapper form-group\" id=\"nField" + intId + "\"/>");
			var fieldId = $('<input name="id"  type="hidden" value="0"/>');
			var fieldNumber = $('<input name="number"  type="tel" class="form-control only-number" placeholder="'+numberPlaceholder+'" id="nInput'
			+ intId + '" />');
			fieldWrapper.append(fieldId);
			fieldWrapper.append(fieldNumber);
			$("#fieldset1").append(fieldWrapper);
			$('#nInput' + intId).focus();
			formatNumber('#nInput' + intId);
		});

	  $('#addMail').click(function() {
		
			intId++;
			var fieldWrapper = $("<div class=\"fieldwrapper form-group\" id=\"mField" + intId + "\"/>");
			var fieldId = $('<input name="id"  type="hidden" value="0" />');
			var fieldMail = $('<input name="mail"  type="email" class="form-control " placeholder="'+emailPlaceholder+'" id="mInput' + intId + '" />');
			fieldWrapper.append(fieldMail);
			fieldWrapper.append(fieldId);
			$("#fieldset2").append(fieldWrapper);
			$('#mInput' + intId).focus();
			addMail();
		});
					// scripts de formularios en tab1
	function updateTab() {

		$('#li_tab_1').removeClass('active');
		$('#li_tab_2').addClass('active');
		$('#tab1').removeClass('in');
		$('#tab1').removeClass('active');
		$('#tab2').addClass('in');
		$('#tab2').addClass('active');
		$('#search').fadeIn(1000);
		$('#addNewContact').css("display", "block");
	}

		$('#li_tab_1').click(function() {
			$('#search').fadeOut(1000);
		});

		$('#li_tab_2').click(function() {
			$('#search').fadeIn(1000);
		});

		function formToJsonString(formId) {

			var data = $(formId).serialize().split("&");
			var comma = '",';
			var jsonString = '{';
			for ( var key in data) {
				if (key == (data.length - 1)) {
					comma = '"';
				}
				var tmpValue = data[key].replace('=', '":"');
				jsonString += '"' + tmpValue + comma;
			}

			return jsonString;
		}

	function formToJsonArrayString(formId, arrayName) {

		var data = $(formId).serialize().split("&");
		var comma = '';
		var jsonArray = arrayName;
		var i = 1;
		for ( var key in data) {
			if (data[key].substr((data[key].length - 1),
					data[key].length) == '=') {
				if (data[key].includes('number')) {
					data[key] = 'number=empty';
				} else if (data[key].includes('zypCode')) {
					data[key] = 'zypCode=empty';
				}
			}
				if ((arrayName.includes('numberList') || arrayName.includes('mailList')) && key > 0 && i % 2 == 0
						|| arrayName.includes('addressList') && key > 0 && i % 5 == 0) {
				comma = '"},{';
				} else {
					comma = '",';
				}
				if (key == (data.length - 1)) {
					comma = '';
				}
				var tmpValue = data[key].replace('=', '":"');
				jsonArray += '"' + tmpValue + comma;
				i++;
			}

			return jsonArray += '"}]';
		}

		$('#contact_next').click(function() {
			form1AsJson = formToJsonString("#contactForm");
			$("#form1").fadeOut(1000, function() {
				$("#form2").fadeIn(1000, function() {
					$('#nInput0').focus();
					formatNumber('#nInput' + intId);
				});
			});
		});

		$('#number_next').click(function() {
					form2AsJson = formToJsonArrayString(
					'#numberForm', ',"numberList":[{');
					$("#form2").fadeOut(1000, function() {
						$("#form3").fadeIn(1000, function() {
							$('#street').focus();
						});
					});
				});

		$('#address_next').click(function() {
			
					form3AsJson = formToJsonArrayString(
					'#addressForm', ',"addressList":[{');
					$("#form3").fadeOut(1000, function() {
						$("#form4").fadeIn(1000, function() {
							$('#mInput0').focus();
							addMail();
						});
					});
				});
					
	$('.btn-cancel-contact').click(function() {
		
				document.getElementById("contactForm").reset();
				document.getElementById("numberForm").reset();
				document.getElementById("addressForm").reset();
				document.getElementById("mailForm").reset();
				$('#form1').css('display', 'none');
				$('#form2').css('display', 'none');
				$('#form3').css('display', 'none');
				$('#form4').css('display', 'none');
				$('#a_tab_2').click();
				setTimeout(function(){ $('#addNewContact').css("display", "block"); }, 500);
				
			});
					

	$('#submit_contact').click(function() {
		
		form4AsJson = formToJsonArrayString(
		'#mailForm', ',"mailList":[{') + '}';
		contactAsJson = form1AsJson + form2AsJson + form3AsJson + form4AsJson;

		contactAsJson = contactAsJson.replace(/"id":"0"/g, '"id":0');
		contactAsJson = contactAsJson.replace(/%40/g, '@');
		contactAsJson = contactAsJson.replace(/\+/g, ' ');
		$('#contactTable').css('display','none');

			var promise = $.ajax({
				
				type : "POST",
				url : "/SaveContacts/User.html",
				data : "contactAsJson=" + contactAsJson + "&username=" + currentUserName
			});
			
			promise.then(function(response) {
				
				$('#table_body').html('');
		        updateTab();
		        getUserContacts();
		       
		        document.getElementById('tableSize').innerHTML = response;

		$("#form4").fadeOut(100,function() {
			
			document.getElementById("contactForm").reset();
			document.getElementById("numberForm").reset();
			document.getElementById("addressForm").reset();
			document.getElementById("mailForm").reset();
		 });

	 });
 });

		$('#back_to_contact').click(function() {
			$('#form2').fadeOut(400, function() {
				$('#form1').fadeIn();
			});
		});
		$('#back_to_number').click(function() {
			$('#form3').fadeOut(400, function() {
				$('#form2').fadeIn();
			});
		});
		$('#back_to_address').click(function() {
			$('#form4').fadeOut(400, function() {
				$('#form3').fadeIn();
			});
		});
		// fin scripts de formularios en tab1

		// scripts para elementos de la tabla

		var tmpIdUpdate = '';
		var tmpIdRemove = '';

		function resizeInput() {
			$(document).on('keyup', '.table_field', function(event) {
				var newSize = $(this).val();
				newSize = (newSize.length * 8) + 'px';
				$(this).css('width', newSize);
			});
		}
		function trimInputs() {
			var inputsList = document.querySelectorAll('input.table_field');
			for (var int = 0; int < inputsList.length; int++) {

				var tmpId = "#" + inputsList[int].id;

				var newSize = $(tmpId).val();
				newSize = (newSize.length * 8) + 'px';
				$(tmpId).css('width', newSize);
				$(tmpId).css('display', 'block');

			}

		}
		document.addEventListener('mouseover', function(event) {

			var currentBtnId = event.target.id;
			if (typeof currentBtnId === "string" &&  currentBtnId.includes("contact_update")) {
				tmpIdUpdate = '#' + currentBtnId;
				$(tmpIdUpdate).css('color', 'green');
				window.setTimeout(function() {
					$('[data-toggle="tooltip"]').tooltip('hide');
				}, 3000);

			} else {
				$(tmpIdUpdate).css('color', 'gray');
			}

			if (typeof currentBtnId === "string" &&  currentBtnId.includes("contact_remove")) {
				tmpIdRemove = '#' + currentBtnId;
				$(tmpIdRemove).css('color', '#F15454');
				window.setTimeout(function() {
					$('[data-toggle="tooltip"]').tooltip('hide');
				}, 3000);

			} else {
				$(tmpIdRemove).css('color', 'gray');
			}
			if (typeof currentBtnId === "string" &&  currentBtnId.includes("addContact")) {
				window.setTimeout(function() {
					$('[data-toggle="tooltip"]').tooltip('hide');
				}, 3000);

			}
		});

	$(document).on('mouseenter','tr',function() {
		
		var rowId = '#' + $(this).attr('id');
		if (typeof rowId === "string" && rowId.includes('contact')) {
			var fieldRowClass = '.'+ $(rowId).children('td').attr('id');
			$(fieldRowClass).css('background-color','rgb(240, 240, 240)');

			$(rowId).find('td').each(function() {
						$(this).css('background-color',	'rgb(240, 240, 240)');
					});

		}
	});
	
	$(document).on('mouseleave','tr',function() {
		
		var rowId = '#' + $(this).attr('id');
		if (typeof rowId === "string" && rowId.includes('contact')) {
			var fieldRowClass = '.' + $(rowId).children('td').attr('id');
			$(fieldRowClass).css('background-color','#f9f9f9');

			$(rowId).find('td').each(function() {
				$(this).css('background-color','#f9f9f9');
		});

		}
	});

	function ajaxContactUpDate(rowId) {
		
		rowId = rowId.substr(1, rowId.length);

		var numberInputObj, numberId, emailInput, emailId;

		var tdNumber = document.getElementById(rowId).getElementsByClassName('tdNumber');
		var tdNumberInputs = tdNumber[0].getElementsByTagName('input');
		var updatedNumbersList = [ [], [] ];
		var tdEmail = document.getElementById(rowId).getElementsByClassName('tdEmail');
		var tdEmailInputs = tdEmail[0].getElementsByTagName('input');
		var updatedEmailsList = [ [], [] ];
		if (tdNumberInputs.length > 0) {

		for (var i = 0; i < tdNumberInputs.length; i++) {
			// agregando el valor de cada imput al array
			if (tdNumberInputs[i].value != '') {
				updatedNumbersList[0][i] = tdNumberInputs[i].value;
			} else {
				updatedNumbersList[0][i] = 'empty';
			}
			// agregando el id de cada telefono al array
			numberInputObj = tdNumberInputs[i];
			numberId = $(numberInputObj).attr('id');
			numberId = numberId.substr(6, numberId.length);
			updatedNumbersList[1][i] = numberId;

			}
		}
		if (tdEmailInputs.length > 0) {

			for (var m = 0; m < tdEmailInputs.length; m++) {
				// agregando el valor de cada imput al array
				if (tdEmailInputs[m].value != '') {
					updatedEmailsList[0][m] = tdEmailInputs[m].value;
				} else {
					updatedEmailsList[0][m] = 'empty';
				}
				// agregando el id de cada email al array
				emailInput = tdEmailInputs[m];
				emailId = $(emailInput).attr('id');
				emailId = emailId.substr(4, emailId.length);
				updatedEmailsList[1][m] = emailId;
			}
		}
	rowId = rowId.substr(7, rowId.length);

	var updatedName = $('#name' + rowId).val();
	var updatedLastName = $('#surname' + rowId).val();
	var updatedGender = $(
	'#currentGender' + rowId + ' option:selected').text();

	var updatedAddress = [ 5 ];
	updatedAddress[0] = rowId;
	updatedAddress[1] = $('#street' + rowId).val();
	updatedAddress[2] = $('#city' + rowId).val();
	updatedAddress[3] = $('#state' + rowId).val();
	updatedAddress[4] = $('#zypCode' + rowId).val();

	var promise = $.ajax({
		type : "POST",
		url : "/SaveContacts/update.html",
		data : "contactId=" + rowId + "&name="
				+ updatedName + "&surname="
				+ updatedLastName + "&gender="
				+ updatedGender + "&numbers="
				+ updatedNumbersList + "&addresses="
				+ updatedAddress + "&mails="
				+ updatedEmailsList + "&currentUserName="
				+ currentUserName
	});
	
	promise.then(function(response) {
		$('#table_body').html('');
		getUserContacts();
	});
  }

	function ajaxContactRemove(contactId) {

	  var tableLength = document.getElementById("contactTable").rows.length;
						
	   var promise = $.ajax({
		   
			type : "POST",
			url : "/SaveContacts/remove.html",
			data : "contactId=" + contactId+ "&tableLength=" + tableLength
		});
	   
	   promise.then(function(response) {
		   
		  document.getElementById('tableSize').innerHTML = response;
	});
  }

	$(document).on('click','.contact_update',function() {
						
		var rowId = '#' + $(this).closest("tr").attr("id");
		var updateId = $(this).children('pre').attr("id");
		var updateClass = $(this).children('pre').attr('class');
		var nameId = $(rowId).find('input').attr('id');
		var inputsClass = $(rowId).children('td').attr('id');

			if (updateClass == 'glyphicon glyphicon-pencil') {
				$('#' + updateId).attr("class","glyphicon glyphicon-floppy-disk");
											
				$(rowId).find('pre.contact-info').each(function() {
					$(this).fadeOut(800, function() {
						$(rowId).find('div.inputDiv').each(function() {
							$(this).fadeIn(800, function() {
								
				// scripts para reposicionar el cursor
			$.fn.setCursorPosition = function(pos) {
				
				$('#' + nameId).each(function(index,elem) {
			    if (elem.setSelectionRange) {elem.setSelectionRange(pos,pos);
				} else if (elem.createTextRange) {
					
		       	var range = elem.createTextRange();
					range.collapse(true);
					range.moveEnd('character',pos);
					range.moveStart('character',pos);
					range.select();
				}
			});
		return this;
	};
															
				var length = $('#' + nameId).val().length;
				$('#' + nameId).focus();
				$('#' + nameId).setCursorPosition(length);
				// fin de scripts para reposicionar el cursor

				});
			});
		});
	});
											
	} else {
		$('#' + updateId).attr("class",	"glyphicon glyphicon-pencil");

			ajaxContactUpDate(rowId);
		}

	});

	$(document).on('click', '.contact_remove', function() {
		var rowId = $(this).closest("tr").attr("id");
		var contactId = rowId.substr(7, rowId.length);
		$('#' + rowId).fadeOut(800, function() {
			$('#' + rowId).remove();
			ajaxContactRemove(contactId);
		});

	});

	$(document).on('focusin', '.table_field', function() {
		var id = $(this).attr('id');
		if (id.includes('number')) {

			formatNumber('#' + id);
		}

	});

	// scripts para la barra de busqueda
	function filterContact() {
		var input, filter, table, trId, trs, fullName, color, i;
		input = document.getElementById("search");
		filter = input.value.toLowerCase();
		filter = filter.replace(/\s/g, '');
		tableBody = document.getElementById("table_body");
		trs = tableBody.getElementsByTagName("tr");
		color = 0;

		for (i = 0; i < trs.length; i++) {
			trId = '#' + trs[i].id;
			var int = trId.substr(8, trId.length);
		    fullName = $("#name" + int).val() + $("#surname" + int).val();
			fullName = fullName.replace(/\s/g, '');
			if (fullName != '') {
				if (!filter) {
					$('#search').css('border-color', '#ccc');
					$('#table_body').find('tr').each(function() {
								$(this).css('display', '');
							});
				} else {

			if (fullName.toLowerCase().indexOf(filter) > -1) {
				color++;
				trs[i].style.display = "";
			} else {
				trs[i].style.display = "none";
			}
			if (color > 0) {
				$('#search').css('border-color','#1F971F');
			} else {
				$('#search').css('border-color', 'red');
			}
		}
	} else {
		trs[i].style.display = "none";
	}
}
						
	//Cambia de color black a green el string encontrado en los p de name y surname
    $('#table_body').find("pre.contactIndex").each(function(){
        var start, end, start_text, end_text, match;
        
        start = $(this).text().toLowerCase().indexOf(filter);
        if(start !== -1){
           
        	 end = filter.length;
             start_text = start != 0 ? $(this).text().substring(0,start) : "";
             end_text = $(this).text().substring(start + end);
             match = $(this).text().substring(start, start + end);
             var tmpHTML = $(this).html();
             var tmpValue = $(this).text().toLowerCase();
             if(tmpHTML.includes('highlight')){
                 tmpHTML = tmpHTML.replace(new RegExp('<span class="highlight">', 'g'), '');
                 tmpHTML = tmpHTML.replace(new RegExp('</span>', 'g'), '');
                 
            	 $(this).html(tmpHTML);
             } 
             if(!tmpHTML.includes('highlight')){
            	 $(this).html(tmpHTML.replace(new RegExp(match, 'g'), '<span class="highlight">'+match+'</span>'));
            	 }
        }
    });
   }
		$('#search').keyup(function() {
			filterContact();
		});

		// fin de scripts para la barra de busqueda

		// fin de scripts para elementos de la tabla
	});