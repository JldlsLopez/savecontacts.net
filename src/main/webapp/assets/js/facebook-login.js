//Facebook graph api version 2.8
function statusChangeCallback(response, justLogin) {
	
	if (response.status === 'connected') {

		$.when($.ajax(goToFBApi(justLogin))).then(function() {
			$("#loading_modal").modal('hide');

		});
	} else if (response.status === 'not_authorized') {

	} else {

		$("#loading_modal").modal('hide');
		if (requestJsp == 'login') {
			requestJsp = '';
			$('#loginHome').click();
		}

	}
}


function checkLoginState(justLogin, domIsReady) {
	
	        if(domIsReady == true){
	        	$("#modal_div").addClass('waitingForFB');
	        	$("#modal_div").modal('hide');
	        	
	        }
       
			FB.getLoginStatus(function(response) {
				activator = true;
				statusChangeCallback(response, justLogin);
			});
}

window.fbAsyncInit = function() {
	FB.init({
		appId : '317376965324074',
		cookie : true,
		xfbml : true,
		version : 'v2.8'
	});
	
	FB.Event.subscribe('xfbml.render', function(response) {
		if(document.getElementById('hidenRequestJsp').innerHTML == 'home'){checkLoginState(true, false);}
		if(document.getElementById('hidenRequestJsp').innerHTML == 'login'){$("#loading_modal").modal('hide');}
	  });
	
};

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id))
		return;
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function goToFBApi(justLogin) {
	FB.api('/me?fields=id,first_name,last_name,email', function(response1) {
		var promise = $.ajax({

			type : "POST",
			url : "FBsignup.html",
			data : "id=" + response1.id + "&first_name=" + response1.first_name
					+ "&last_name=" + response1.last_name + "&email="
					+ response1.email + "&justLogin=" + justLogin
		});

		promise.then(function(response2) {
			response2 = response2.replace(/'/g, '"');
			var user = JSON.parse(response2);
			$(document).ready(
					function() {

						if (user.userNameIsSaved == 'true' & user.justLogin == 'true') {

							document.getElementById('fbLoginForm').reset();
							document.getElementById('fbUsrNameLogin')
									.setAttribute("value", user.userName);
							document.getElementById('fbPswLogin').setAttribute(
									"value", user.password);
							document.getElementById('fbLoginForm').submit();
						}
					});
		});

	});

}

//logout
function statusChangeCallback2(response) {

  if (response.status === 'connected') {

  	appAndFBLogout();
  	
  }else {
  	document.getElementById('user_logout_form').submit();
  }
}

function checkLoginState2() {
	$("#loading_modal").modal({backdrop: "static"});
  FB.getLoginStatus(function(response) {
    statusChangeCallback2(response);
  });
}
function appAndFBLogout() {//reemplazar este metodo por un algoritmo que no afecte el estado (logged/not logged) de la cuenta de fb del usuario
	  $.when($.ajax(FB.logout())).then(function() {
		  document.getElementById('user_logout_form').submit();
		  
	  });

}

